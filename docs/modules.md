# Modules

These modules have been created for a couple of reasons, to demonstrate how to create and use modules for
a variety of tasks, and to use during development and testing.

## Messaging

This module handles the various RPC calls that are defined by `plantd`.

```shell
docker build -t plantd-messaging -f modules/messaging/Dockerfile modules/messaging
docker run -it plantd-messaging
```
