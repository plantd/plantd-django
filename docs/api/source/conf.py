# Configure for Django
import os
import sys

import django

sys.path.insert(0, os.path.abspath("../.."))

os.environ["DJANGO_SETTINGS_MODULE"] = "config.settings.documentation"
django.setup()

project = "plantd"
copyright = "2020, Geoff Johnson"
author = "Geoff Johnson"

# The full version, including alpha/beta/rc tags
release = "0.1.0"


# General configuration
extensions = [
    "sphinx.ext.autodoc",
    "sphinx.ext.autosectionlabel",
    "sphinx.ext.autosummary",
    "sphinx.ext.doctest",
    "sphinx.ext.coverage",
    "sphinx.ext.intersphinx",
    "sphinx.ext.viewcode",
]

autodoc_default_options = {
    "members": True,
    "show-inheritance": True,
    "undoc-members": True,
}

autosectionlabel_prefix_document = True
autosectionlabel_maxdepth = 2
autosummary_generate = True

templates_path = ["_templates"]

exclude_patterns = [
    "build",
    ".venv",
    ".DS_Store",
]

pygments_style = "sphinx"

intersphinx_mapping = {
    "https://docs.python.org/3": None,
    "https://docs.djangoproject.com/en/1.11": "https://docs.djangoproject.com/en/1.11/_objects/",
}


# Options for HTML output
html_theme = "sphinx_rtd_theme"
html_static_path = ["_static"]
