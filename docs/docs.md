# Documentation

This space is for auto-generated documentation using `sphinx` as well as developer
oriented content as a series of Markdown files.

## Generate API Site

Eventually documentation should be built during CI, but until that happens the
manual build steps are given here.

```shell
cd docs/api
make apidocs
make watch
```

The documentation pages should be available at http://localhost:9001.

## Developer Pages

Table of contents:

* [Develop](develop.md)
* [Modules](modules.md)
