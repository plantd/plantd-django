# Develop

## Setup

The quick start on the [README](README.md) doesn't include some other steps, like setting up a virtual environment. You
can do this however you like, but this is a suggestion that uses the version of Python that has been tested.

### Environment

This will setup a Python `virtualenv` and install the packages required for development. This assumes that `pyenv` and
`direnv` have been installed using the method given by those tools for installation on the OS being used, this may
differ but the common installation for both uses `curl` and is given on their respective Github project pages.

```shell
cp .env.development .env
direnv allow
PYTHON_VERSION="$(cat .python-version)"
pyenv install $PYTHON_VERSION
pyenv virtualenv $PYTHON_VERSION plantd
pyenv activate plantd
pip install --upgrade pip
pip install -r requirements.txt
pip install -r requirements.dev.txt
poetry install
```

### Docker

Start the additional services. If you want to use `fusionauth` for authentication the appropriate keys for whatever
provider should be used should be added to `.env`, these instructions don't include that yet. It's not required that
any additional authentication be added for development.

```shell
docker-compose build
docker-compose up -d
```

### Database

Connecting to the database running within a container might fail when there's no volume connected for the run PID,
create that with `sudo mkdir /var/run/postgresql`.

A database needs to exist that matches the configuration, log in using the credentials that have been set in the `.env`
file.

```shell
docker-compose exec db psql -U postgres -h localhost
```

At the `postgres=#` prompt create the `plantd` database and exit.

```sql
create database plantd;
```

### Launch

It's recommended that you create a local configuration file at `config/settings/local.py` with the contents:

```python
"""Personalized configuration settings."""

from config.settings.development import *

log = structlog.get_logger(__name__)

if DEBUG:
    LOGGING["loggers"]["plantd"]["level"] = "DEBUG"
    log.info("DEBUG enabled")

# If using FusionAuth change these to whatever is configured there
FUSION_AUTH_APP_ID = "..."
FUSION_AUTH_CLIENT_SECRET = "..."
FUSION_AUTH_API_KEY = "..."
FUSION_AUTH_BASE_URL = "http://localhost:9011"
```

This will be used for local development, but it needs to be specified when launching using the `manage.py` file. If you
don't want to do that you could add `export DJANGO_SETTINGS_MODULE=config.settings.local` to your environment.

```shell
./manage.py migrate
DJANGO_SETTINGS_MODULE=config.settings.local ./manage.py createsuperuser
DJANGO_SETTINGS_MODULE=config.settings.local ./manage.py runserver
```

The `createsuperuser` management command will ask for a username, email, and password. They can be whatever but one
suggestion is:

* admin
* admin@plantd.net
* plantd

Note that the suggested credentials will result in a warning that the password is too short and too similar to the
email address; this can be safely ignored for the development setup.

### RabbitMQ

A RabbitMQ broker should be running as a container, for some reason it doesn't load the management plugin even with the
container image that's supposed to include it so that needs to be started manually.

```shell
docker-compose exec broker rabbitmq-plugins enable rabbitmq_management
```

Check the status of the broker with:

```shell
docker-compose exec broker rabbitmqctl status
```

The management interface should be available at http://localhost:15672/ now, the default user/pass is set in the
`docker-compose.yml` file as`rabbit`/`rabbit`.

### FusionAuth

At this point you can visit http://localhost:9011/maintenance-mode-database-configuration if you choose to attempt the
setup for some authentication service. The username and password that needs to be input on that page are what were set
for the PostgreSQL database in the `.env` file.

TODO: More instructions are required before this is useful, the instructions that were used to set it all up once can be
found [here][fusionauth-django].

### User Interface

If you're on Linux the installation for `cypress` may fail, if that's the case replace the `yarn install` command with
`CHILD_CONCURRENCY=1 yarn install`.

```shell
cd app
yarn install
yarn serve
```

The user interface should be running at http://localhost:8080.

#### Storybook

Run the component `storybook` with `yarn run storybook:serve`.

### Next Steps

Everything should be up and running at this point and you should be able to login at http://localhost:8000/admin using
the superuser credentials that were created. Other useful links:

* [GraphiQL][graphiql] IDE to access the GraphQL API
* [RabbitMQ][rabbitmq] broker
* [UI][vue-ui]
* [Storybook][storybook]

## Updates

To use `pyup` to keep dependencies up to date:

```shell
pyup --provider gitlab --repo=plantd/plantd-django --user-token=$PYUP_TOKEN --branch=staging
```

## Generate GraphQL Schema

This can be useful for integrating with an IDE, eg. PyCharm using the JS GraphQL Plugin.

```shell
./manage.py graphql_schema --schema plantd.schema.schema --out schema.schema
```

<!-- links -->

[fusionauth-django]: https://fusionauth.io/blog/2020/07/14/django-and-oauth/
[graphiql]: http://localhost:8000/graphql
[rabbitmq]: http://localhost:15672
[vue-ui]: http://localhost:8080
[storybook]: http://localhost:6006
