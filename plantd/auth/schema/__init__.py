"""GraphQL schema for the authentication app."""

# flake8: noqa

from .mutation import Mutation
from .query import Query
