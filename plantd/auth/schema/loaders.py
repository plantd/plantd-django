from django.contrib.auth.models import User
from promise import Promise
from promise.dataloader import DataLoader


class UserLoader(DataLoader):
    """Data loader for users."""

    def batch_load_fn(self, keys):
        """Load a set of users for the keys that are passed in.

        :param keys: List of user IDs
        :return: Promise that resolves the list of users that were requested
        """
        users = {user.id: user for user in User.objects.filter(id__in=keys)}
        return Promise.resolve([users.get(user_id) for user_id in keys])
