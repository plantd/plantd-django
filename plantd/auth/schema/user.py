"""GraphQL types for the authentication app."""

import graphene
from django.contrib.auth import get_user_model
from graphene import relay
from graphene_django import DjangoObjectType

from plantd.core.models import Profile


class UserNode(DjangoObjectType):
    """Node for a `User`."""

    class Meta:
        model = get_user_model()
        filter_fields = ["username", "email"]
        interfaces = (relay.Node,)


class CreateUser(graphene.Mutation):
    """Mutation to create a new `User`."""

    user = graphene.Field(UserNode)

    class Arguments:
        username = graphene.String(required=True)
        password = graphene.String(required=True)
        email = graphene.String(required=True)

    def mutate(self, info, username, password, email):
        """Create a new user and return it."""
        user = get_user_model()(
            username=username,
            email=email,
        )
        user.set_password(password)
        user.save()

        # create a profile that's associated with the user
        profile = Profile(user=user)
        profile.save()

        return CreateUser(user=user)
