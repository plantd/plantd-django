"""GraphQL query provider for the authentication app."""

import graphene
from django.contrib.auth import get_user_model
from graphene_django.filter import DjangoFilterConnectionField

from .user import UserNode


class Query(graphene.AbstractType):
    """Authentication queries."""

    users = DjangoFilterConnectionField(UserNode)
    me = graphene.Field(UserNode)

    def resolve_users(self, info):
        """
        Resolver for a `User`

        :param info: Query information
        :returns: `User`
        """
        return get_user_model().objects.all()

    def resolve_me(self, info):
        """
        Resolver for the `User` of the user performing the query

        :param info: Query information
        :returns: `User`
        """
        user = info.context.user
        if user.is_anonymous:
            raise Exception("Not logged in!")

        return user
