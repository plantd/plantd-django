"""GraphQL mutation provider for the authentication app."""

import graphene

from .user import CreateUser


class Mutation(graphene.AbstractType):
    """Authentication mutations."""

    create_user = CreateUser.Field()
