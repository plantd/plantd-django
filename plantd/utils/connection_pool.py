import queue
import threading
import time
from functools import partial

from django.conf import settings
from zapi import mdp


class TooManyConnections(Exception):
    pass


class Expired(Exception):
    pass


class UsageExceeded(Expired):
    pass


class TTLExceeded(Expired):
    pass


class IdleExceeded(Expired):
    pass


class ConnectionWrapper(object):
    def __init__(self, connection_pool, connection):
        self.pool = connection_pool
        self.connection = connection
        self.usage = 0
        now = time.time()
        self.last = now
        self.created = now

    def using(self):
        self.usage += 1
        self.last = time.time()
        return self

    def reset(self):
        self.usage = 0
        self.last = 0
        self.created = 0

    def __enter__(self):
        return self.connection

    def __exit__(self, *exc_info):
        self.pool.release(self)


class ConnectionPool(object):
    """
    Generic connection pool that could be used for databases, or in our case
    plantd ZMQ clients.

    standard connection：

    .. code-block:: python

       pool = ConnectionPool(create=zapi.mdp.Client)

    connection using a lambda：

    .. code-block:: python

       pool = ConnectionPool(create=lambda: zapi.mdp.Client(broker="tcp://localhost:7200"))

    connection using functools.partial

    .. code-block:: python

       from functools import partial
       pool = ConnectionPool(create=partial(zapi.mdp.Client, broker="tcp://localhost:7200"))
    """

    __wrappers = {}

    def __init__(self, create, close=None, max_size=10, max_usage=0, ttl=0, idle=60, block=True):
        """Initialize a new connection pool instance.

        :param create: callback to create a new connection
        :param close: optional callback to close connections
        :param max_size: max number of connections, 0 for no limit (not recommended)
        :param max_usage: max number of times a connection can be used before being closed
        :param ttl: connection time to live in seconds, closed when exceeded
        :param idle: connection idle time in seconds, closed when exceeded
        :param block: whether or not to block when max connections is reached, use `False` to throw exception when full
        """
        if not hasattr(create, "__call__"):
            raise ValueError("'create' argument is not callable")

        if close is not None and not hasattr(close, "__call__"):
            raise ValueError("'close' argument is not callable")

        self._create = create
        self._close = close
        self._max_size = int(max_size)
        self._max_usage = int(max_usage)
        self._ttl = int(ttl)
        self._idle = int(idle)
        self._block = bool(block)
        self._lock = threading.Condition()
        self._pool = queue.Queue()
        self._size = 0

    def item(self):
        """Get the next item in the pool to use in `with ... as ...`

        .. code-block:: python

           pool = ConnectionPool(create=zapi.mdp.Client)
           with pool.item() as client:
               client.send(b"foo", [b"get-property", b"{\"key\":\"foo\"}"])
        """
        self._lock.acquire()

        try:
            while self._max_size and self._pool.empty() and self._size >= self._max_size:
                if not self._block:
                    raise TooManyConnections("Too many connections")

                self._lock.wait()

            try:
                wrapped = self._pool.get_nowait()
                if self._idle and (wrapped.last + self._idle) < time.time():
                    self._destroy(wrapped)
                    raise IdleExceeded(f"Idle exceeded {self._idle} secs")
            except (queue.Empty, IdleExceeded):
                wrapped = self._wrapper(self._create())
                self._size += 1
        finally:
            self._lock.release()

        return wrapped.using()

    def release(self, conn):
        self._lock.acquire()
        wrapped = self._wrapper(conn)

        try:
            self._test(wrapped)
        except Expired:
            self._destroy(wrapped)
        else:
            self._pool.put_nowait(wrapped)
            self._lock.notifyAll()
        finally:
            self._lock.release()

    def _destroy(self, wrapped):
        if self._close:
            self._close(wrapped.connection)

        self._unwrapper(wrapped)
        self._size -= 1

    def _wrapper(self, conn):
        if isinstance(conn, ConnectionWrapper):
            return conn

        _id = id(conn)

        if _id not in self.__wrappers:
            self.__wrappers[_id] = ConnectionWrapper(self, conn)

        return self.__wrappers[_id]

    def _unwrapper(self, wrapped):
        if not isinstance(wrapped, ConnectionWrapper):
            return

        _id = id(wrapped.connection)
        wrapped.reset()
        del wrapped

        if _id in self.__wrappers:
            del self.__wrappers[_id]

    def _test(self, wrapped):
        if self._max_usage and wrapped.usage >= self._max_usage:
            raise UsageExceeded(f"Usage exceeds {self._max_usage} times")

        if self._ttl and (wrapped.created + self._ttl) < time.time():
            raise TTLExceeded(f"TTL exceeds {self._ttl} secs")


pool = ConnectionPool(create=partial(mdp.Client, broker=settings.BROKER["endpoint"]))
