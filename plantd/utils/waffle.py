"""Waffle feature flag and switch names."""

flags = [
    "sample-feature-flag",
]

switches = [
    "sample-feature-switch",
]
