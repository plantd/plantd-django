"""Custom decorators."""
from functools import wraps


class DocInherit:
    """Decorator to inherit documentation from a parent with @doc_inherit.

    This is a modified version of what's offered by the ``custom-inherit``
    package.
    """

    def __init__(self, method):
        self.method = method
        self.name = method.__name__

    def __get__(self, obj, cls):
        """Get the parent documentation with or without the class instance."""
        if obj:
            return self._get_with_instance(obj, cls)
        return self._get_without_instance(cls)

    def _get_with_instance(self, obj, cls):
        """Get the docstring when an instance has been provided."""
        overridden = getattr(super(cls, obj), self.name, None)

        @wraps(self.method, assigned=("__name__", "__module__"))
        def f(*args, **kwargs):
            return self.method(obj, *args, **kwargs)

        return self.use_parent_doc(f, overridden)

    def _get_without_instance(self, cls):
        """Get the docstring when no instance has been provided."""
        overridden = None

        for parent in cls.__mro__[1:]:
            overridden = getattr(parent, self.name, None)
            if overridden:
                break

        @wraps(self.method, assigned=("__name__", "__module__"))
        def f(*args, **kwargs):
            return self.method(*args, **kwargs)

        return self.use_parent_doc(f, overridden)

    def use_parent_doc(self, func, source):
        """
        Copy the parent documentation into the function.

        :param func: Function to copy documentation to
        :param source: Parent to copy the documentation from
        :return: The function updated with parent documentation
        """
        if source is None:
            raise NameError(f"Cannot find '{self.name}' in parents")
        func.__doc__ = source.__doc__
        return func


doc_inherit = DocInherit
