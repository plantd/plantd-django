from enum import Enum

from django.utils.encoding import force_text


class EnumHelper(Enum):
    @classmethod
    def choices(cls, enum):
        return [(option.value, force_text(option.name)) for option in enum]

    @classmethod
    def has_value(cls, enum, value):
        return value in (option.value for option in enum)

    @classmethod
    def option_from_value(cls, enum, value):
        for option in enum:
            if option.value == value:
                return option
        return None
