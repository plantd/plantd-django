"""GraphQL utility functions and types."""

from typing import Any

from graphene import Scalar


def get_query_cost(schema, operation, variables, fragments):
    """
    Calculate a GraphQL query cost.
    """
    # TODO: implement calculation
    return 0, {"__total": 0}


def get_required_scopes(schema, operation, variables, fragments):
    """
    Identify the scopes required from a GraphQL query.
    """
    # TODO: implement scope check
    return ["all"]


class SerializedObject(Scalar):
    """Serialized object to wrap a basic type."""

    @staticmethod
    def serialize(value: Any) -> Any:
        """Passes the `value` through."""
        return value
