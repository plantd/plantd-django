import fnmatch
import logging
import os
import re
from collections import OrderedDict

import yaml

logger = logging.getLogger(__name__)

# Match "${foo.bar}" and "${foo}" where resulting matches would be
# ("foo.", "bar") and ("", "foo"). "${foo.bar.baz}" will not be matched.
TMPL_PATTERN = re.compile(r".*?\${(\w+\.)?(\w+)}.*?")

# Pattern to match an environment variable inside $(ENV)
ENV_PATTERN = re.compile(r".*?\$\((\w+)\).*?")


class Loader(yaml.SafeLoader):
    """YAML Loader with `!include` and `!include_dir` constructors."""

    def __init__(self, stream):
        """Initialize Loader."""

        # if the stream contains template data attach it to the loader
        if hasattr(stream, "template_data"):
            self.template_data = stream.template_data

        try:
            self._root = os.path.split(stream.name)[0]
        except AttributeError:
            self._root = os.path.curdir

        super().__init__(stream)


def _load_yaml(filename, template_data=None):
    """Load a YAML file."""
    try:
        with open(filename, encoding="utf-8") as file:
            if template_data:
                file.template_data = template_data
            # load the file as an empty dictionary if it's empty
            return yaml.load(file, Loader) or OrderedDict()
    except (UnicodeDecodeError, yaml.YAMLError) as e:
        logger.error("Unable to read file %s: %s", filename, e)
        raise e


def _find_files(directory, pattern):
    """Load files in a directory that match `pattern`."""
    files = os.listdir(directory)
    for basename in sorted(files):
        if fnmatch.fnmatch(basename, pattern):
            filename = os.path.join(directory, basename)
            yield filename


def _include(loader, node):
    """Include a file referenced at `node`."""

    value = loader.construct_scalar(node)
    template_data = None
    if hasattr(loader, "template_data"):
        template_data = loader.template_data
    filename = os.path.abspath(os.path.join(loader._root, value))
    extension = os.path.splitext(filename)[1].lstrip(".")
    if extension in ("yaml", "yml"):
        return _load_yaml(filename, template_data)


def _include_dir(loader, node):
    """Include a directory of files referenced at `node` as a list."""
    value = loader.construct_scalar(node)
    template_data = None
    if hasattr(loader, "template_data"):
        template_data = loader.template_data
    directory = os.path.abspath(os.path.join(loader._root, value))
    return [_load_yaml(file, template_data) for file in _find_files(directory, "*.y*ml")]


def _template(loader, node):
    """Search and replace known template strings."""
    value = loader.construct_scalar(node)
    match = TMPL_PATTERN.findall(value)
    if match and hasattr(loader, "template_data"):
        full_value = value
        try:
            for group in match:
                # the field on the pattern always has a result, the object is optional
                field = group[1]
                if group[0] != "":
                    # the first match includes the "."
                    obj_name = group[0][:-1]
                    obj = loader.template_data[obj_name]
                    repl_field = f"{obj_name}.{field}"
                    full_value = full_value.replace(f"${{{repl_field}}}", str(getattr(obj, field)))
                else:
                    full_value = full_value.replace(f"${{{field}}}", str(loader.template_data[field]))
        except IndexError as e:
            raise Exception(f"The requested template data does not exist: {e}")
        return full_value
    return value


def _env(loader, node):
    """Replace an environment variable following !env and inside $(ENV)."""
    value = loader.construct_scalar(node)
    match = ENV_PATTERN.findall(value)
    if match:
        full_value = value
        for group in match:
            full_value = full_value.replace(f"$({group})", os.environ.get(group, group))
        return full_value
    return value


def setup_loader():
    """Add the constructor(s) to the YAML loader."""

    # support loading separate files into one document
    yaml.add_constructor("!include", _include, Loader)
    yaml.add_constructor("!include_dir", _include_dir, Loader)

    # process template strings to allow for replacing eg. domain names in data
    yaml.add_implicit_resolver("!tmpl", TMPL_PATTERN, None, Loader)
    yaml.add_constructor("!tmpl", _template, Loader)

    # support environment variables
    yaml.add_implicit_resolver("!env", ENV_PATTERN, None, Loader)
    yaml.add_constructor("!env", _env, Loader)
