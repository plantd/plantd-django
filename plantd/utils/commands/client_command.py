from django.core.management.base import BaseCommand, CommandError
from zapi.mdp import Client


class ClientCommand(BaseCommand):
    def __init__(self, stdout=None, stderr=None, no_color=False, force_color=False):
        super().__init__(stdout, stderr, no_color, force_color)
        self.endpoint = None
        self.service = None
        self.client = None

    def add_arguments(self, parser):
        parser.add_argument("service", type=str)
        parser.add_argument("endpoint", type=str)

    def handle(self, *args, **options):
        self.endpoint = options["endpoint"]
        self.service = options["service"].encode()
        self.client = Client(self.endpoint, verbose=True)

        try:
            self._handle(*args, **options)
        except KeyboardInterrupt:
            print("\nAborted")
        except CommandError:
            raise

    def _handle(self, *args, **options):
        pass
