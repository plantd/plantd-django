"""GraphQL test utilities."""
from typing import Any, Dict, List
from unittest.mock import MagicMock

import structlog
from django.http import HttpRequest
from django.urls import reverse_lazy

from plantd.core.schema.loaders import ObjectLoader, ProfileLoader, PropertiesByObjectPkLoader, PropertyLoader
from plantd.schema import schema

log = structlog.get_logger(__name__)

graphql_path = reverse_lazy("graphql")


class MockRequest(MagicMock):
    """Mock request to execute a GraphQL query with, only need post."""

    def __init__(self, method="GET", path="", user=None):
        super().__init__(spec=HttpRequest)
        self.profile_loader = ProfileLoader()
        self.object_loader = ObjectLoader()
        self.property_loader = PropertyLoader()
        self.properties_by_object_pk_loader = PropertiesByObjectPkLoader()

    @classmethod
    def post(cls, *args, **kwargs):
        """Perform a POST request."""
        return cls("POST", *args, **kwargs)


def run_query(requestee, query: str, fragments: List[str] or None = None, variables: Dict[str, Any] = None):
    """Execute a GraphQL query.

    :param requestee: The profile to perform the query as.
    :param query: Query to execute.
    :param fragments: An optional list of fragments to include in the query.
    :param variables: Variables to use with the query.
    :return: Result of the executed query.
    """
    request = MockRequest.post(graphql_path, user=requestee.user)
    if fragments is not None:
        fragments.append(query)
        query = "".join(fragments)
    result = schema.execute(query, context_value=request, variable_values=variables)
    if result.errors:
        raise AssertionError(f"Received GraphQL errors: {result.errors}")
    return result.data
