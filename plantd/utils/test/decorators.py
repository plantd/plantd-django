from django.conf import settings


def ci_skip(func):
    def wrapper(*args, **kwargs):
        if not settings.CI:
            func(*args, **kwargs)

    return wrapper
