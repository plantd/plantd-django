"""Test runner to include feature flags."""
import os

import structlog
from django.test.runner import _init_worker, DiscoverRunner, ParallelTestSuite
from waffle.models import Flag, Switch

from plantd.utils import is_true

log = structlog.get_logger(__name__)


def _plantd_init_worker(counter):
    """Called when tests are run under certain conditions, eg. --parallel."""
    _init_worker(counter)
    _init_waffle()


def _init_waffle():
    """Initialize waffle flags and switches."""
    from plantd.utils.waffle import flags, switches

    for flag in flags:
        Flag.objects.create(name=flag)

    for switch in switches:
        Switch.objects.create(name=switch)


class PlantdParallelTestSuite(ParallelTestSuite):
    """Includes waffle flags and switches."""

    init_worker = _plantd_init_worker


class PlantdTestRunner(DiscoverRunner):
    """Plantd test runner.

    TODO: for now this doesn't do anything, it should be used eventually for
     doing things like loading feature flags
    """

    parallel_test_suite = PlantdParallelTestSuite

    def __init__(self, pattern=None, keepdb=False, **kwargs):
        log.debug("using local test runner")

        if pattern == "test*.py":
            pattern = "*"
        if is_true(os.environ.get("TEST_KEEP_DB")):
            keepdb = True

        super().__init__(pattern=pattern, keepdb=keepdb, **kwargs)
