"""Utilities to work with the Django user model."""
from django.contrib.auth.models import User


def get_or_create_user(user_id: int, request=None) -> User:
    """
    Retrieve a user, or create one if none exists for the ID provided.

    :param user_id:
    :param request:
    :return: `django.contrib.auth.models.User`
    """
    user = User.objects.filter(username=user_id).first()

    if not user:
        user = User(username=user_id)
        user.save()

    return user
