"""Utilities to support authorization tasks."""

from typing import Union

import pkce
import structlog
from django.conf import settings
from django.shortcuts import reverse
from fusionauth.fusionauth_client import FusionAuthClient

from plantd.utils.user import get_or_create_user

log = structlog.get_logger(__name__)


def get_login_url(request) -> str:
    """
    Create a login URL for the OAuth client.

    :param request: `django.http.HttpRequest`
    :return: The login URL
    """
    redirect_url = request.build_absolute_uri(reverse("profile"))
    base_url = settings.FUSION_AUTH_BASE_URL
    app_id = settings.FUSION_AUTH_APP_ID
    login_url = f"{base_url}/oauth2/authorize?client_id={app_id}&redirect_uri={redirect_url}&response_type=code"
    if "pkce_verifier" in request.session or "code_challenge" in request.session:
        code_verifier = pkce.generate_code_verifier(length=128)
        code_challenge = pkce.get_code_challenge(code_verifier)
        request.session["pkce_verifier"] = code_verifier
        request.session["code_challenge"] = code_challenge
        login_url = f"{login_url}&code_challenge={request.session['code_challenge']}&code_challenge_method=S256"
    return login_url


def is_user_login_ok(request) -> Union[str, bool]:
    """
    Check if the user associated with the oauth code can be exchanged for an
    access token.

    :param request: `django.http.HttpRequest`
    :return: User ID string if the access token can be received, False if there's a failure
    """
    client = FusionAuthClient(settings.FUSION_AUTH_API_KEY, settings.FUSION_AUTH_BASE_URL)

    code = request.GET.get("code")
    if not code:
        log.warn("no code was received in the request")
        return False

    try:
        redirect_url = request.build_absolute_uri(reverse("profile"))
        if "pkce_verifier" in request.session:
            r = client.exchange_o_auth_code_for_access_token_using_pkce(
                code,
                redirect_url,
                request.session["pkce_verifier"],
                settings.FUSION_AUTH_APP_ID,
                settings.FUSION_AUTH_CLIENT_SECRET,
            )
        else:
            r = client.exchange_o_auth_code_for_access_token(
                code,
                settings.FUSION_AUTH_APP_ID,
                redirect_url,
                settings.FUSION_AUTH_CLIENT_SECRET,
            )

        if r.was_successful():
            # XXX: taken from example application, not sure what to do with it yet
            # access_token = r.success_response["access_token"]
            user_id = r.success_response["userId"]
            get_or_create_user(user_id, request)
            return user_id
        else:
            log.warn("could not exchange code for token", error=r.error_response)
            return False

    except Exception as e:
        log.error("an error occurred while exchanging oauth code for access token", error=e)
