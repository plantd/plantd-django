from typing import Dict

from plantd.core.factories import ObjectFactory, OrganizationFactory, ProfileFactory, PropertyFactory, UserFactory
from plantd.core.factories.configuration import ConfigurationFactory
from plantd.core.models import Object, Organization, Profile
from plantd.devices.factories.device import DeviceFactory
from plantd.networks.factories.network import NetworkFactory
from plantd.networks.models import Network


def create_organization(organization: Dict):
    """Create an organization using input that's (currently) loaded from YAML data."""
    _organization = OrganizationFactory(name=organization["name"])
    for user in organization.get("users", []):
        create_profile(user, _organization)
    for network in organization.get("networks", []):
        create_network(network, _organization)
    for configuration in organization.get("configurations", []):
        create_configuration(configuration, _organization)
    return _organization


def create_profile(user: Dict, organization: Organization):
    """Create a profile using input that's (currently) loaded from YAML data."""
    permissions = user.get("permissions", {})
    user_args = {
        "username": user.get("username", None),
        "first_name": user.get("first_name", None),
        "last_name": user.get("last_name", None),
        "email": user.get("email", None),
        "password": user.get("password", None),
        "is_staff": permissions.get("staff", None),
        "is_active": permissions.get("active", None),
        "is_superuser": permissions.get("superuser", None),
    }
    kwargs = {k: v for k, v in user_args.items() if v is not None}
    _user = UserFactory(**kwargs)
    _user.save()
    profile = ProfileFactory(user=_user, organization=organization)
    profile.save()
    return profile


def create_network(network: Dict, organization: Organization):
    """Create a network using input that's (currently) loaded from YAML data."""
    _network = NetworkFactory(name=network["name"], organization=organization)
    for device in network.get("devices", []):
        create_device(device, _network)
    return _network


def create_object(obj: Dict, creator: Profile, parent: Object = None):
    """Create an object using input that's (currently) loaded from YAML data."""
    _obj = ObjectFactory(name=obj["name"], creator=creator, parent=parent)
    for prop in obj.get("properties", []):
        create_property(prop, _obj)
    for obj in obj.get("objects", []):
        create_object(obj, creator=creator, parent=_obj)
    return _obj


def create_property(prop: Dict, obj: Object):
    """Create a property using input that's (currently) loaded from YAML data."""
    kv = [(k, v) for k, v in prop.items()]
    _prop = PropertyFactory(object=obj, key=kv[0][0])
    _prop.value = kv[0][1]
    _prop.save()
    return _prop


def create_configuration(configuration: Dict, organization: Organization, creator: Profile = None):
    """Create a configuration using input that's (currently) loaded from YAML data."""
    _configuration = ConfigurationFactory(name=configuration["name"], organization=organization)
    if creator is None:
        creator = organization.profiles.first()
    create_object(configuration["root"], creator=creator, parent=_configuration.root)
    return _configuration


def create_dashboard(dashboard: Dict, organization: Organization):
    """Create a dashboard using input that's (currently) loaded from YAML data."""
    _dashboard = None
    return _dashboard


def create_device(device: Dict, network: Network):
    """Create a device using input that's (currently) loaded from YAML data."""
    return DeviceFactory(name=device["name"], network=network)
