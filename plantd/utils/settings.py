"""Django settings utilities."""
import os
import sys

import structlog

log = structlog.get_logger(__name__)


def _get_settings_module() -> str:
    """Get the appropriate settings module from the environment."""
    if os.environ.get("DJANGO_SETTINGS_MODULE"):
        log.info("using module set by environment variable")
        return os.environ.get("DJANGO_SETTINGS_MODULE")
    elif "test" in sys.argv:
        log.info("using config.settings.testing")
        return "config.settings.testing"
    else:
        log.info("using production settings")
        return "config.settings.production"


def django_settings_module(settings_module: str = None) -> None:
    """
    Set the settings module that will be loaded.

    :param settings_module:  eg. "config.settings.production"
    :return:
    """
    if "DJANGO_SETTINGS_MODULE" not in os.environ:
        if settings_module is None:
            settings_module = _get_settings_module()
        os.environ.setdefault("DJANGO_SETTINGS_MODULE", settings_module)


def django_setup(settings_module: str = None) -> None:
    """
    Used to initialize Django settings to make it possible to load outside of
    the app, eg. jupyter.

    :param settings_module: eg. "config.settings.production"
    """
    import django

    django_settings_module(settings_module)
    django.setup()
