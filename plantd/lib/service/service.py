import threading

import structlog
from zapi import mdp

log = structlog.get_logger(__name__)


class Service:
    def __init__(self, name: str, mq_endpoint: str, max_workers=3):
        self.running = False
        self.jobs = []
        self.name = name
        self.mq_endpoint = mq_endpoint
        self.workers = []

        for i in range(max_workers):
            log.info(f"creating thread for: {i}")
            thread = threading.Thread(target=self._handle, args=(i,))
            self.jobs.append(thread)

    def start(self):
        log.info("starting jobs")
        self.running = True
        for job in self.jobs:
            job.start()

    def stop(self):
        log.info("stopping jobs")
        self.running = False
        for job in self.jobs:
            job.join()

    def _handle(self, _id):
        worker = mdp.Worker(broker=self.mq_endpoint, service=self.name.encode())
        reply = None
        log.info(f"handler for: {_id}")

        while self.running:
            log.info(f"worker {_id}: receive")
            request = worker.recv(reply)
            if len(request) == 0:
                log.warning(f"worker {_id}: invalid request")
                continue

            log.info(f"worker {_id}: {request[0]}")
            if request[0].decode() == "echo":
                reply = request
            else:
                reply = None

        worker.destroy()
