from dataclasses import dataclass

import jsons


@dataclass
class Serializable:
    def to_json(self) -> str:
        return jsons.dumps(
            self,
            indent=2,
            key_transformer=jsons.KEY_TRANSFORMER_LISPCASE,
        )

    @classmethod
    def from_json(cls, json: str) -> dataclass:
        return jsons.loads(
            json,
            cls,
            strict=False,
            key_transformer=jsons.KEY_TRANSFORMER_SNAKECASE,
        )
