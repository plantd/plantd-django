# Types from the protobuf definitions that are needed for messaging
from __future__ import annotations

from dataclasses import dataclass
from typing import List, Optional, Union

from .serializable import Serializable


@dataclass
class Property(Serializable):
    key: str
    value: Optional[Union[str, int, float, bool]]


@dataclass
class Object(Serializable):
    id: str
    properties: Optional[List[Property]]
    objects: Optional[List[Object]]


@dataclass
class Job(Serializable):
    id: str
    priority: int = 0
