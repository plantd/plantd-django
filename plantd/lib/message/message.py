from dataclasses import dataclass

from ..types import Serializable


@dataclass
class Message(Serializable):
    pass
