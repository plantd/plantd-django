from dataclasses import dataclass

from ..types import Job
from .message import Message


@dataclass
class JobResponse(Message):
    job: Job
