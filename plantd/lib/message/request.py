from dataclasses import dataclass
from typing import List, Optional

from ..types import Property
from .message import Message


@dataclass
class JobRequest(Message):
    id: str
    job_id: Optional[str] = None
    job_value: Optional[str] = None
    job_properties: Optional[List[Property]] = None
