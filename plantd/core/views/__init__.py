"""Core views."""

from .graphql import *  # noqa
from .home import *  # noqa
from .logout import *  # noqa
from .profile import *  # noqa
