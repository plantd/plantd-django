"""Profile view."""
import dateparser
import structlog
from django.conf import settings
from django.shortcuts import redirect, render
from django.views.generic import View
from fusionauth.fusionauth_client import FusionAuthClient

from plantd.utils.auth import get_login_url, is_user_login_ok

log = structlog.get_logger(__name__)


class ProfileView(View):
    """Profile view to allow user to read and edit profile settings."""

    @staticmethod
    def get(request):
        """
        Retrieve profile information.

        :param request:
        :return:
        """
        login_url = get_login_url(request)
        user_id = is_user_login_ok(request)
        if not user_id:
            return redirect(login_url)

        birthday = None

        try:
            client = FusionAuthClient(settings.FUSION_AUTH_API_KEY, settings.FUSION_AUTH_BASE_URL)
            log.info("read user profile", user_id=user_id)
            r = client.retrieve_user(user_id)

            if r.was_successful():
                birthday = r.success_response["user"].get("birthDate", "unknown")
                log.info("received birthday", birthday=birthday)
            else:
                log.warn("couldn't get user", error=r.error_response)
            log.info("render dashboard with ", user_id=user_id)
            return render(request, "profile.html", {"birthday": birthday, "user_id": user_id})
        except Exception as e:
            log.warn("couldn't get user", error=e)
            return redirect(login_url)

    @staticmethod
    def post(request):
        """
        Handle POST request to save profile information.

        :param request:
        :return:
        """

        birthday = request.POST.get("birthday")
        user_id = request.POST.get("user_id")
        normalised_birthday = None
        log.info("received profile", user_id=user_id, birthday=birthday)

        try:
            dt = dateparser.parse(birthday)
            normalised_birthday = dt.strftime("%Y-%m-%d")
        except Exception as e:
            log.warn("Couldn't parse birthday", error=e)

        if not normalised_birthday:
            return render(
                request,
                "profile.html",
                {"message": "Couldn't parse birthday. Please use YYYY-MM-DD", "user_id": user_id},
            )

        try:
            client = FusionAuthClient(settings.FUSION_AUTH_API_KEY, settings.FUSION_AUTH_BASE_URL)
            r = client.patch_user(user_id, {"user": {"birthDate": normalised_birthday}})
            if r.was_successful():
                log.info("successfully updated user profile", response=r.success_response)
                return render(
                    request,
                    "profile.html",
                    {
                        "message": "Updated your birthday",
                        "birthday": normalised_birthday,
                        "user_id": user_id,
                    },
                )

            else:
                log.warn("failed to update user profile", error=r.error_response)
                return render(
                    request,
                    "profile.html",
                    {"message": "Something went wrong", "user_id": user_id},
                )

        except Exception as e:
            log.error("failed to update user profile", error=e)
            return render(
                request,
                "profile.html",
                {"message": "Something went wrong"},
            )
