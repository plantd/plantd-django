"""Views for GraphQL to provide login and authorization support."""

from django.utils.functional import cached_property
from graphene_django.views import GraphQLView

from plantd.core.schema.loaders import ObjectLoader, ProfileLoader, PropertiesByObjectPkLoader, PropertyLoader


class GraphQLContext:
    def __init__(self, request):
        self.request = request
        self.META = request.META
        self.COOKIES = request.COOKIES

    @cached_property
    def user(self):
        return self.request.user

    @cached_property
    def profile_loader(self):
        return ProfileLoader()

    @cached_property
    def object_loader(self):
        return ObjectLoader()

    @cached_property
    def property_loader(self):
        return PropertyLoader()

    @cached_property
    def properties_by_object_pk_loader(self):
        return PropertiesByObjectPkLoader()


# class PrivateGraphQLView(LoginRequiredMixin, GraphQLView):
class PrivateGraphQLView(GraphQLView):
    """Private GraphQL view"""

    def get_context(self, request):
        return GraphQLContext(request)


class PublicGraphQLView(GraphQLView):
    """Public GraphQL view"""

    pass
