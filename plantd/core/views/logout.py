"""Logout view."""
from django.conf import settings
from django.shortcuts import redirect
from django.views.generic import View


class LogoutView(View):
    """Logout view for auth management."""

    @staticmethod
    def get(request, *args, **kwargs):
        """
        GET request handler for logging a profile out by redirecting to OAuth provider.

        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        request.session.clear()
        # XXX: taken from example app, not sure what to do with it yet
        # redirect_url = request.build_absolute_uri("home")
        url = f"{settings.FUSION_AUTH_BASE_URL}/oauth2/logout?client_id={settings.FUSION_AUTH_APP_ID}"
        return redirect(url)
