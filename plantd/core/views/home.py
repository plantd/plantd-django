"""Home view."""

from django.contrib.auth.models import User
from django.http import HttpRequest, HttpResponse
from django.shortcuts import render
from django.views.generic import View

from plantd.utils.auth import get_login_url


class HomeView(View):
    """Default view for the application."""

    @staticmethod
    def get(request: HttpRequest) -> HttpResponse:
        """
        Handle GET requests.

        :param request: Incoming request
        :return: Rendered home page
        """
        num_users = User.objects.count()
        login_url = get_login_url(request)
        return render(
            request,
            "home.html",
            {"login_url": login_url, "num_users": num_users},
        )
