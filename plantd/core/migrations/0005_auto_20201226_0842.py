# Generated by Django 3.1.4 on 2020-12-26 08:42

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("core", "0004_configuration_object_property"),
    ]

    operations = [
        migrations.RenameField(
            model_name="property",
            old_name="bool_value",
            new_name="_bool_value",
        ),
        migrations.RenameField(
            model_name="property",
            old_name="float_value",
            new_name="_float_value",
        ),
        migrations.RenameField(
            model_name="property",
            old_name="int_value",
            new_name="_int_value",
        ),
        migrations.RenameField(
            model_name="property",
            old_name="str_value",
            new_name="_str_value",
        ),
        migrations.AlterField(
            model_name="property",
            name="_bool_value",
            field=models.BooleanField(
                blank=True,
                db_column="bool_value",
                default=None,
                help_text="Property value as a floating point number.",
                null=True,
                verbose_name="boolean value",
            ),
        ),
        migrations.AlterField(
            model_name="property",
            name="_float_value",
            field=models.FloatField(
                blank=True,
                db_column="float_value",
                default=None,
                help_text="Property value as a floating point number.",
                null=True,
                verbose_name="float value",
            ),
        ),
        migrations.AlterField(
            model_name="property",
            name="_int_value",
            field=models.IntegerField(
                blank=True,
                db_column="int_value",
                default=None,
                help_text="Property value as an integer.",
                null=True,
                verbose_name="integer value",
            ),
        ),
        migrations.AlterField(
            model_name="property",
            name="_str_value",
            field=models.CharField(
                blank=True,
                db_column="str_value",
                default=None,
                help_text="Property value as a string.",
                max_length=150,
                null=True,
                verbose_name="string value",
            ),
        ),
        migrations.AlterUniqueTogether(
            name="property",
            unique_together={("key", "object")},
        ),
        migrations.RemoveField(
            model_name="property",
            name="configuration",
        ),
    ]
