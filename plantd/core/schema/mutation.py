"""GraphQL mutation provider for the core app."""

from graphene import AbstractType

from .object import CreateObject
from .property import CreateProperty


class Mutation(AbstractType):
    """Core mutations."""

    create_object = CreateObject.Field()
    create_property = CreateProperty.Field()
