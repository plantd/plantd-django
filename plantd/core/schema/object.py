import graphene
from graphene import relay
from graphene_django import DjangoConnectionField, DjangoObjectType

from plantd.core.models import Object

from .profile import ProfileNode
from .property import PropertyInput, PropertyUnionConnection


class ObjectNode(DjangoObjectType):
    """GraphQL type for an object."""

    class Meta:
        model = Object
        fields = ["name"]
        interfaces = (relay.Node,)

    pk = graphene.Int()
    name = graphene.String()
    creator = graphene.Field(ProfileNode)
    objects = DjangoConnectionField(lambda: ObjectNode)
    properties = graphene.ConnectionField(PropertyUnionConnection)

    def resolve_objects(root, info):
        # TODO: create loader that returns children
        return []

    def resolve_properties(root, info):
        return info.context.properties_by_object_pk_loader.load(root.pk)


class ObjectInput(graphene.InputObjectType):
    """Input type for `Object` mutations."""

    name = graphene.String(description="The object name")
    properties = graphene.List(
        PropertyInput,
        description="List of properties for the object",
    )


class CreateObject(graphene.Mutation):
    """Mutation to create a new `Object.`"""

    object = graphene.Field(ObjectNode)

    class Arguments:
        name = graphene.String(required=True)

    def mutate(self, info, name):
        """Create a new objet and return it."""
        profile = info.context.user.profile
        object = Object.objects.create(name=name, creator=profile)

        return CreateObject(object)
