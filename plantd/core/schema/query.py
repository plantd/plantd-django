"""GraphQL query provider for the core app."""
import graphene
from graphene import AbstractType
from graphene_django import DjangoConnectionField
from graphene_django.filter import DjangoFilterConnectionField

from .object import ObjectNode
from .profile import ProfileNode
from .property import PropertyUnionNode


class Query(AbstractType):
    """Core GraphQL queries."""

    object = graphene.Field(ObjectNode, pk=graphene.Int(required=True))
    objects = DjangoConnectionField(ObjectNode, pks=graphene.List(graphene.Int))
    profile = graphene.Field(ProfileNode, pk=graphene.Int(required=True))
    profiles = DjangoFilterConnectionField(ProfileNode, pks=graphene.List(graphene.Int))
    property = graphene.Field(PropertyUnionNode, pk=graphene.Int(required=True))
    # properties = DjangoConnectionField(PropertyUnionNode, pks=graphene.List(graphene.Int))

    def resolve_profile(root, info, pk):
        return info.context.profile_loader.load(pk)

    def resolve_object(root, info, pk):
        return info.context.object_loader.load(pk)
