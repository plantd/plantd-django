import graphene
from graphene import AbstractType, relay
from graphene_django import DjangoObjectType
from graphql import GraphQLError

from plantd.core.models import Object, Property, PropertyType


class PropertyNode(AbstractType):
    """GraphQL type for a property."""

    key = graphene.String()


class IntProperty(PropertyNode, DjangoObjectType):
    class Meta:
        model = Property
        interfaces = (relay.Node,)
        only_fields = ["key", "value"]

    value = graphene.Int()


class StringProperty(PropertyNode, DjangoObjectType):
    class Meta:
        model = Property
        interfaces = (relay.Node,)
        only_fields = ["key", "value"]

    value = graphene.String()


class FloatProperty(PropertyNode, DjangoObjectType):
    class Meta:
        model = Property
        interfaces = (relay.Node,)
        only_fields = ["key", "value"]

    value = graphene.Float()


class BoolProperty(PropertyNode, DjangoObjectType):
    class Meta:
        model = Property
        interfaces = (relay.Node,)
        only_fields = ["key", "value"]

    value = graphene.Boolean()


class PropertyUnionNode(graphene.Union):
    """Union type for goal types to use with connections."""

    class Meta:
        types = (
            IntProperty,
            StringProperty,
            FloatProperty,
            BoolProperty,
        )

    @classmethod
    def resolve_type(cls, instance, info):
        """Determine the node type to return for a given property type."""
        if instance.value_type == PropertyType.INTEGER.value:
            return IntProperty
        elif instance.value_type == PropertyType.STRING.value:
            return StringProperty
        elif instance.value_type == PropertyType.FLOAT.value:
            return FloatProperty
        elif instance.value_type == PropertyType.BOOLEAN.value:
            return BoolProperty
        return None


class PropertyUnionConnection(graphene.Connection):
    """Connection that provides the goal node union type."""

    class Meta:
        node = PropertyUnionNode


class PropertyInput(graphene.InputObjectType):
    """Input type for `Property` mutations."""

    key = graphene.String(description="Property key")
    int_value = graphene.Int(description="Integer property value")
    str_value = graphene.String(description="String property value")
    float_value = graphene.Float(description="Float property value")
    bool_value = graphene.Boolean(description="Boolean property value")


class CreateProperty(graphene.Mutation):
    """Mutation to create a new `Property`."""

    property = graphene.Field(PropertyUnionNode)

    class Arguments:
        object_pk = graphene.Int()
        input = graphene.Argument(PropertyInput)

    def mutate(self, info, object_pk, input):
        """Create a new `Property` and return it."""
        object = Object.objects.get(pk=object_pk)
        property = Property(key=input.key, object=object)
        value = list(
            filter(
                lambda value: value is not None,
                [input.int_value, input.str_value, input.float_value, input.bool_value],
            )
        )
        if len(value) != 1:
            raise GraphQLError("Only one of the value types should be supplied a value")
        property.value = value[0]
        property.save()

        return CreateProperty(property)
