"""GraphQL schema for the core app."""

# flake8: noqa

from .mutation import Mutation
from .query import Query
