import graphene
from graphene import relay
from graphene_django import DjangoObjectType

from plantd.auth.schema.user import UserNode
from plantd.core.models import Profile


class ProfileNode(DjangoObjectType):
    """GraphQL type for a profile."""

    class Meta:
        model = Profile
        fields = ["user"]
        filter_fields = ["id", "user__username"]
        interfaces = (relay.Node,)

    full_name = graphene.String()
    first_name = graphene.String()
    last_name = graphene.String()
    user = graphene.Field(UserNode)

    def resolve_user(root, info):
        return root.user
