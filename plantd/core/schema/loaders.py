from collections import defaultdict

from promise import Promise
from promise.dataloader import DataLoader

from plantd.core.models import Object, Profile, Property


class ProfileLoader(DataLoader):
    """Data loader for profiles."""

    def batch_load_fn(self, keys):
        """Load a set of profiles for the keys that are provided.

        :param keys: List of profile IDs
        :return: Promise that resolves the list of profiles that were requested
        """
        profiles = {profile.pk: profile for profile in Profile.objects.filter(pk__in=keys)}
        return Promise.resolve([profiles.get(profile_pk) for profile_pk in keys])


class ObjectLoader(DataLoader):
    """Data loader for objects."""

    def batch_load_fn(self, keys):
        """Load a set of objects for the keys that are provided.

        :param keys: List of object IDs
        :return: Promise that resolves the list of objects that were requested.
        """
        objects = {object.pk: object for object in Object.objects.filter(pk__in=keys)}
        return Promise.resolve([objects.get(object_pk) for object_pk in keys])


class PropertyLoader(DataLoader):
    """Data loader for properties."""

    def batch_load_fn(self, keys):
        """Load a set of properties for the keys that are provided.

        :param keys: List of property IDs
        :return: Promise that resolves the list of properties that were requested.
        """
        properties = {property.pk: property for property in Property.objects.filter(pk__in=keys)}
        return Promise.resolve([properties.get(property_pk) for property_pk in keys])


class PropertiesByObjectPkLoader(DataLoader):
    """Data loader to fetch the properties for a set of objects."""

    def batch_load_fn(self, object_pks):
        """Load a set of object properties for the object keys that are provided.

        :param keys: List of property IDs
        :return: Promise that resolves the list of properties for the objects that were requested.
        """
        properties_by_object_pks = defaultdict(list)
        for property in Property.objects.filter(object_id__in=object_pks).iterator():
            properties_by_object_pks[property.object_id].append(property)
        return Promise.resolve([properties_by_object_pks.get(object_pk, []) for object_pk in object_pks])
