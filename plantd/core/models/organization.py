"""Organization model and associated manager classes."""

from django.db import models
from django.utils.translation import ugettext_lazy as _

from plantd.core.validators import OrganizationNameValidator

ORGANIZATION_NAME_MAX_LENGTH = 150


class OrganizationManager(models.Manager):
    """Model manager for organizations."""

    pass


class Organization(models.Model):
    """Model class for organizations that profiles and networks belong to."""

    class Meta:
        app_label = "core"

    name_validator = OrganizationNameValidator()

    name = models.CharField(
        _("name"),
        max_length=ORGANIZATION_NAME_MAX_LENGTH,
        unique=True,
        null=False,
        blank=True,
        default=None,
        help_text=_("Required. 150 characters or fewer. Letters, and digits only."),
        validators=[name_validator],
        error_messages={"unique": _("An organization with that name already exists.")},
    )

    objects = OrganizationManager()
