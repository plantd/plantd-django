"""Property model and associated manager classes."""
from enum import Enum
from typing import List, Tuple, Union

from django.db import models
from django.utils.encoding import force_text
from django.utils.translation import ugettext_lazy as _

PROPERTY_KEY_MAX_LENGTH = 150
PROPERTY_VALUE_MAX_LENGTH = 150


class PropertyValueError(Exception):
    pass


class PropertyType(Enum):
    """Available property types."""

    INTEGER = 1
    STRING = 2
    FLOAT = 3
    BOOLEAN = 4

    @classmethod
    def choices(cls) -> List[Tuple[int, str]]:
        """A list of value and name tuples for the enum."""
        return [(e.value, force_text(e.name)) for e in cls]


class PropertyManager(models.Manager):
    pass


class Property(models.Model):
    """Generic property model used to describe configurations and objects."""

    class Meta:
        app_label = "core"
        unique_together = ["key", "object"]

    key = models.CharField(
        _("key"),
        max_length=PROPERTY_KEY_MAX_LENGTH,
        null=False,
        blank=True,
        default=None,
        help_text=_(f"Required. {PROPERTY_KEY_MAX_LENGTH} characters or fewer."),
    )

    object = models.ForeignKey(
        "core.Object",
        related_name="properties",
        on_delete=models.CASCADE,
        help_text=_("Object that the property is associated with."),
    )

    _int_value = models.IntegerField(
        _("integer value"),
        default=None,
        null=True,
        blank=True,
        db_column="int_value",
        help_text=_("Property value as an integer."),
    )

    _str_value = models.CharField(
        _("string value"),
        max_length=PROPERTY_KEY_MAX_LENGTH,
        default=None,
        null=True,
        blank=True,
        db_column="str_value",
        help_text=_("Property value as a string."),
    )

    _float_value = models.FloatField(
        _("float value"),
        default=None,
        null=True,
        blank=True,
        db_column="float_value",
        help_text=_("Property value as a floating point number."),
    )

    _bool_value = models.BooleanField(
        _("boolean value"),
        default=None,
        null=True,
        blank=True,
        db_column="bool_value",
        help_text=_("Property value as a floating point number."),
    )

    value_type = models.SmallIntegerField(
        _("Property value type."),
        choices=PropertyType.choices(),
        default=PropertyType.INTEGER.value,
        help_text=_("Type of the property value."),
    )

    @property
    def value(self) -> Union[str, int, float, bool]:
        """Retrieves the property value for the currently assigned type."""
        return {
            PropertyType.INTEGER.value: self._int_value,
            PropertyType.STRING.value: self._str_value,
            PropertyType.FLOAT.value: self._float_value,
            PropertyType.BOOLEAN.value: self._bool_value,
        }[self.value_type]

    objects = PropertyManager()

    @value.setter
    def value(self, val: Union[str, int, float, bool]):
        """Sets the property value using the input type."""
        saved_value = self.value
        self._int_value = None
        self._str_value = None
        self._float_value = None
        self._bool_value = None
        # Boolean needs to be checked before int
        if isinstance(val, bool):
            self._bool_value = val
            self.value_type = PropertyType.BOOLEAN.value
        elif isinstance(val, int):
            self._int_value = val
            self.value_type = PropertyType.INTEGER.value
        elif isinstance(val, str):
            self._str_value = val
            self.value_type = PropertyType.STRING.value
        elif isinstance(val, float):
            self._float_value = val
            self.value_type = PropertyType.FLOAT.value
        else:
            self.__set_value_for_type(saved_value, self.value_type)
            raise PropertyValueError("The value provided is not a supported property type.")

    def value_and_type(self) -> Tuple[str, Union[str, int, float, bool]]:
        """Convenience function to get the value and the type."""
        return self.value, self.value_type

    def __set_value_for_type(self, value, value_type):
        """Convenience function to set the value for a known type."""
        if value_type == PropertyType.INTEGER.value:
            self._int_value = value
        elif value_type == PropertyType.STRING.value:
            self._str_value = value
        elif value_type == PropertyType.FLOAT.value:
            self._float_value = value
        elif value_type == PropertyType.BOOLEAN.value:
            self._bool_value = value

    def __str__(self):
        return f"{self.key}: {self.value}"
