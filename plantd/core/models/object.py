"""Object model and associated manager classes."""
from enum import Enum

from django.db import models
from django.utils.translation import ugettext_lazy as _

from plantd.utils import EnumHelper

OBJECT_NAME_MAX_LENGTH = 150


class ObjectType(Enum):
    """Supported object types."""

    meta = 0


class ObjectManager(models.Manager):
    pass


class Object(models.Model):
    """
    Generic object model to be used with various different systems, including:
    * Dashboards
    * Control Systems
    * Data Acquisition
    * Data Logging
    """

    class Meta:
        app_label = "core"

    name = models.CharField(
        _("name"),
        max_length=OBJECT_NAME_MAX_LENGTH,
        null=False,
        blank=True,
        default=None,
        help_text=_(f"Required. {OBJECT_NAME_MAX_LENGTH} characters or fewer."),
    )

    type = models.SmallIntegerField(
        _("Object type"),
        choices=EnumHelper.choices(ObjectType),
        default=ObjectType.meta.value,
    )
    """Object type determines what is used when loaded through a configuration.

    Objects are generic and in configurations can be used for various purposes,
    they can describe widgets in a UI/dashboard, channel information in data
    acquisition, parameters in a feedback control system, etc.
    """

    creator = models.ForeignKey(
        "core.Profile",
        related_name="created_objects",
        editable=False,
        on_delete=models.CASCADE,
        help_text=_("The profile that created this object."),
    )

    parent = models.ForeignKey(
        "core.Object",
        related_name="children",
        null=True,
        on_delete=models.CASCADE,
        help_text=_("The parent object."),
    )

    objects = ObjectManager()

    @property
    def siblings(self):
        """Retrieve all objects with the same parent."""
        return Object.objects.filter(parent=self.parent)

    @property
    def has_children(self):
        """`True` if the object has children, `False` otherwise."""
        return Object.objects.filter(parent=self).exists()

    @property
    def has_parent(self):
        """`True` if the object has a parent, `False` otherwise."""
        return self.parent is not None

    # def is_ancestor(self, obj):
    #     # easy checks first
    #     if obj.parent is None:
    #         return False
    #     if obj.parent == self:
    #         return True
    #     # get root
    #     # for child in children

    def is_descendant(self, obj):
        """
        Check if an object is a descendant of this one.

        :param obj: The :class:``Object`` to check.
        :return: `True` if the object is a descendant, `False` otherwise.
        """
        if obj == self.parent:
            return True
        if self.parent is None:
            return False
        return self.parent.is_descendant(obj)
