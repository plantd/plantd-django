"""Profile model and associated manager classes."""

from datetime import date
from urllib.parse import urljoin

import pytz
from django.conf import settings
from django.contrib.auth import logout
from django.contrib.auth.models import User
from django.db import models
from django.db.models import Q
from django.utils.timezone import now
from django.utils.translation import ugettext_lazy as _

USERNAME_MAX_LENGTH = 150
EMAIL_MAX_LENGTH = 150
DEFAULT_TIMEZONE = "US/Pacific"


class ProfileManager(models.Manager):
    pass


class Profile(models.Model):
    """User profile model that is associated with user accounts."""

    class Meta:
        app_label = "core"

    user = models.OneToOneField(
        User,
        unique=True,
        related_name="profile",
        on_delete=models.CASCADE,
    )

    timezone = models.CharField(
        _("timezone"),
        max_length=32,
        default="",
        blank=True,
        help_text=_("Timezone the profile is in."),
    )

    organization = models.ForeignKey(
        "Organization",
        related_name="profiles",
        default=None,
        null=True,
        on_delete=models.CASCADE,
        help_text=_("Organization that the profile belongs to."),
    )

    objects = ProfileManager()

    @property
    def username(self) -> str:
        """Username of the profile from the associated `User`."""
        return self.user.username

    @property
    def full_name(self) -> str:
        """Full name of the profile from the associated `User`."""
        return self.user.get_full_name()

    @property
    def first_name(self) -> str:
        """First name of the profile from the associated `User`."""
        return self.user.first_name

    @property
    def last_name(self) -> str:
        """Last name of the profile for the associated `User`."""
        return self.user.last_name

    @property
    def email(self) -> str:
        """Email of the profile for the associated `User`."""
        return self.user.email

    @property
    def effective_timezone(self) -> str:
        """Timezone that the profile is in."""
        return self.timezone or DEFAULT_TIMEZONE

    @property
    def tzinfo(self):
        """Information for the timezone associated with the profile."""
        return pytz.timezone(self.effective_timezone)

    @property
    def local_datetime(self):
        """Local date and time for the timezone."""
        return now().astimezone(self.tzinfo)

    @property
    def local_date(self) -> date:
        """Local date for the timezone."""
        return self.local_datetime.date()

    @staticmethod
    def logout(request) -> None:
        """Perform a logout request."""
        logout(request)

    def _username_exists(self, username: str) -> bool:
        q_username = Q(user__username=username)
        q_profile = Q(pk=self.pk)
        if self.pk:
            return Profile.objects.filter(q_username & ~q_profile).exists()
        else:
            return Profile.objects.filter(q_username).exists()

    def delete(self, using=None, keep_parents=False):
        """Must delete the associated :class:``User`` as well."""
        return self.user.delete()

    def get_absolute_url(self) -> str:
        """URL for the profile.

        XXX: may not need this, profile will probably be handled by the application.
        """
        return f"/profile/{self.pk}"

    def get_full_absolute_url(self) -> str:
        """Full URL for the profile including the server string.

        XXX: may not need this, profile will probably be handled by the application.
        """
        return urljoin(settings.SERVER_LOCATION, self.get_absolute_url())

    def __unicode__(self) -> str:
        return f"{self.user.get_full_name()} <{self.user.email}> ({self.user.username})"

    def __str__(self) -> str:
        return f"{self.user.get_full_name()} <{self.user.email}> ({self.user.username})"

    def __repr__(self) -> str:
        return f"<{self.user.__class__.__name__} {self.user.pk}: {self.user}>"


User.userprofile = property(lambda user: Profile.objects.get_or_create(user=user)[0])
