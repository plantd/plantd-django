"""Configuration model and associated manager classes."""
from django.db import models
from django.utils.translation import ugettext_lazy as _

CONFIGURATION_NAME_MAX_LENGTH = 150


class ConfigurationManager(models.Manager):
    pass


class Configuration(models.Model):
    """
    Configuration model for describing various parts of systems, including:
    * Dashboards
    * Control Systems
    * Data Acquisition
    * Data Logging
    """

    class Meta:
        app_label = "core"

    name = models.CharField(
        _("name"),
        max_length=CONFIGURATION_NAME_MAX_LENGTH,
        null=False,
        blank=True,
        default=None,
        help_text=_(f"Required. {CONFIGURATION_NAME_MAX_LENGTH} characters or fewer."),
    )

    organization = models.ForeignKey(
        "core.Organization",
        related_name="configurations",
        on_delete=models.CASCADE,
        help_text=_("Organization that the configuration is associated with."),
    )

    root = models.OneToOneField(
        "core.Object",
        null=True,
        default=None,
        on_delete=models.CASCADE,
        help_text=_("Root node for the configuration."),
    )

    objects = ConfigurationManager()
