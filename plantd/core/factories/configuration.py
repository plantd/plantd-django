import factory

from plantd.core.models import Configuration


class ConfigurationFactory(factory.django.DjangoModelFactory):
    """Factory for constructing instances of the Configuration model."""

    class Meta:
        model = Configuration

    name = factory.Sequence(lambda n: f"config{n}")
