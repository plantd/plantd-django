import factory

from plantd.core.models import Object


class ObjectFactory(factory.django.DjangoModelFactory):
    """Factory for constructing instances of the Object model."""

    class Meta:
        model = Object

    name = factory.Sequence(lambda n: f"object{n}")
