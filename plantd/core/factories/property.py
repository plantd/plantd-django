import factory

from plantd.core.models import Property


class PropertyFactory(factory.django.DjangoModelFactory):
    """Factory for constructing instances of the Property model."""

    class Meta:
        model = Property

    key = factory.Sequence(lambda n: f"key{n}")
