import factory

from plantd.core.models import Organization


class OrganizationFactory(factory.django.DjangoModelFactory):
    """Factory for constructing instances of the Organization model."""

    class Meta:
        model = Organization

    name = factory.Sequence(lambda n: f"organization{n}")
