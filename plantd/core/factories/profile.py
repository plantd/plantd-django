import factory

from plantd.core.models import DEFAULT_TIMEZONE, Profile

from .organization import OrganizationFactory
from .user import UserFactory


class ProfileFactory(factory.django.DjangoModelFactory):
    """Factory for constructing instances of the Profile model."""

    class Meta:
        model = Profile

    user = factory.SubFactory(UserFactory)
    organization = factory.SubFactory(OrganizationFactory)
    timezone = DEFAULT_TIMEZONE
