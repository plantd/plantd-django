import factory
from django.contrib.auth.hashers import make_password
from django.contrib.auth.models import User

default_password = "plantd"
hashed_password = make_password(default_password)


class UserFactory(factory.django.DjangoModelFactory):
    """Factory for constructing instances of the User model."""

    class Meta:
        model = User

    username = factory.Sequence(lambda n: f"user{n}")
    first_name = factory.Sequence(lambda n: "Agent %03d" % n)
    last_name = "Name"
    email = f"{username}@domain.com"
    is_staff = True
    is_active = True
    is_superuser = False
    password = hashed_password
