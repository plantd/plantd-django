"""Model field validators."""

from django.core import validators
from django.utils.deconstruct import deconstructible
from django.utils.translation import gettext_lazy as _


@deconstructible
class OrganizationNameValidator(validators.RegexValidator):
    """Organization names should only be letters, numbers, and @/./+/-/_ characters."""

    regex = r"^[\w.@+-]+\Z"
    message = _(
        "Enter a valid organization name. This value may contain only letters, numbers, and @/./+/-/_ characters."
    )
    flags = 0


@deconstructible
class DeviceNameValidator(validators.RegexValidator):
    """Device names should only be letters, numbers, and ./-/_ characters."""

    regex = r"^[\w.-]+\Z"
    message = _(
        "Enter a valid organization name. This value may contain only letters, numbers, and @/./+/-/_ characters."
    )
    flags = 0
