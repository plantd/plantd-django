"""Core URL patterns."""

from django.urls import path, re_path
from django.views.decorators.csrf import csrf_exempt

from plantd.core.views import HomeView, LogoutView, PrivateGraphQLView, ProfileView, PublicGraphQLView
from plantd.schema import public_schema, schema

# FIXME: this would be nice, but it doesn't agree with headers setting
# from graphene_django.views import GraphQLView


# FIXME: same as reason above, doesn't pass headers properly
# GraphQLView.graphiql_template = "graphene_graphiql_explorer/graphiql.html"

urlpatterns = [
    path("", HomeView.as_view(), name="home"),
    path("profile", ProfileView.as_view(), name="profile"),
    re_path("^logout/?$", LogoutView.as_view(), name="logout"),
    re_path("^graphql/?$", csrf_exempt(PrivateGraphQLView.as_view(graphiql=True, schema=schema)), name="graphql"),
    re_path(
        "^graphql/public/?$",
        csrf_exempt(PublicGraphQLView.as_view(graphiql=True, schema=public_schema)),
        name="public_graphql",
    ),
]
