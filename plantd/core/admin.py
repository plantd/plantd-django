"""Admin classes for the core app."""

from django import forms
from django.contrib import admin
from django.contrib.admin.templatetags.admin_urls import add_preserved_filters
from django.http import HttpResponseRedirect

from plantd.core.models import Profile
from plantd.utils.decorators import doc_inherit


class ProfileForm(forms.ModelForm):
    """Admin form for profiles."""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    @doc_inherit
    def save(self, commit=True):
        result = super().save(commit=commit)
        return result

    class Meta:
        model = Profile
        fields = "__all__"


class ProfileAdmin(admin.ModelAdmin):
    """Admin model for profiles."""

    list_display = ("user",)
    search_fields = ("user__email", "user__first_name", "user__last_name")

    form = ProfileForm

    @doc_inherit
    def get_fieldsets(self, request, obj=None):
        fieldsets = (
            (
                "Info",
                {
                    "fields": [
                        "timezone",
                    ]
                },
            ),
        )
        return fieldsets

    @doc_inherit
    def get_urls(self):
        urls = super().get_urls()
        return urls

    def response_without_message(self, request):
        """Create an HTTP response with preserved filters."""
        redirect_url = request.path
        redirect_url = add_preserved_filters(
            {"preserved_filters": self.get_preserved_filters(request), "opts": self.model._meta}, redirect_url
        )
        return HttpResponseRedirect(redirect_url)

    @doc_inherit
    def save_model(self, request, obj, form, change) -> None:
        result = super().save_model(request, obj, form, change)
        return result


admin.site.register(Profile, ProfileAdmin)
