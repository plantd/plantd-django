from django.contrib.auth.models import User
from django.core.management import BaseCommand

from plantd.core.models import Configuration, Object, Organization, Profile, Property
from plantd.dashboards.models import Dashboard
from plantd.devices.models import Device
from plantd.networks.models import Network


class Command(BaseCommand):
    """
    This destroys everything other than the first admin user that the
    documentation sets up. Don't use it if you don't want to get rid of
    everything in the database.
    """

    def handle(self, *args, **options):
        Configuration.objects.all().delete()
        Property.objects.all().delete()
        Object.objects.all().delete()
        Dashboard.objects.all().delete()
        Profile.objects.all().delete()
        User.objects.filter(id__gt=1).delete()
        Device.objects.all().delete()
        Network.objects.all().delete()
        Organization.objects.all().delete()
