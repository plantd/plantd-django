from plantd.lib.message.request import JobRequest
from plantd.utils.commands.client_command import ClientCommand


class Command(ClientCommand):
    help = "Submits a job using a plantd client"

    def _handle(self, *args, **options):
        job = JobRequest(
            id="test",
            job_id="test-job",
            job_value="test-value",
            job_properties={},
        )
        request = [b"submit-job", job.to_json().encode()]
        reply = self.client.send(self.service, request)
        print(f"received: {reply}")
