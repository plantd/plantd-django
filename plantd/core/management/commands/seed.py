from io import StringIO

import yaml
from django.core.management.base import BaseCommand
from django.db import transaction

from plantd.utils.helpers.model import create_organization
from plantd.utils.yaml import Loader

# should load this from a file eventually
config = """
organization:
  name: Example
  users:
    - username: admin@example
      first_name: Administrator
      last_name: Default
      email: admin@example.com
      password: plantd
      permissions:
        staff: true
        active: true
        superuser: true
  networks:
    - name: Example
      devices:
        - name: messaging
  configurations:
    - name: test
      root:
        name: root
        properties:
          - sval: bar
          - ival: 99
          - bval: true
          - fval: 1.23
        objects:
          - name: foo
            properties:
              - foo: bar
"""


class Command(BaseCommand):
    def handle(self, *args, **options):
        stream = StringIO(config)
        stream.template_data = {}
        data = yaml.load(stream, Loader)
        with transaction.atomic():
            create_organization(data["organization"])
