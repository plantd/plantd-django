import json

from plantd.utils.commands.client_command import ClientCommand


class Command(ClientCommand):
    help = "Requests a property using a plantd client"

    def add_arguments(self, parser):
        super().add_arguments(parser)
        parser.add_argument("key", type=str)

    def _handle(self, *args, **options):
        key = options["key"]
        prop = json.dumps({"id": self.service.decode(), "key": key})
        request = [b"get-property", prop.encode()]
        reply = self.client.send(self.service, request)
        print(f"received: {reply}")
