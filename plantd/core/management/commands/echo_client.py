from plantd.utils.commands.client_command import ClientCommand


class Command(ClientCommand):
    help = "Tests the echo service using a plantd client"

    def _handle(self, *args, **options):
        request = [b"echo", b"test"]
        reply = self.client.send(self.service, request)
        print(f"received: {reply}")
