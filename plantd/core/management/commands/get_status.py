from plantd.utils.commands.client_command import ClientCommand


class Command(ClientCommand):
    help = "Requests the status of a module using a plantd client"

    def _handle(self, *args, **options):
        request = [b"get-status", b"{}"]
        reply = self.client.send(self.service, request)
        print(f"received: {reply}")
