from django.core.management import BaseCommand

from plantd.lib.service import Service


class Command(BaseCommand):
    def __init__(self, stdout=None, stderr=None, no_color=False, force_color=False):
        super().__init__(stdout, stderr, no_color, force_color)
        self.service = Service("echo", "tcp://localhost:7200")

    def handle(self, *args, **options):
        # FIXME: this doesn't work, worker doesn't receive from broker
        try:
            self.service.start()
        except KeyboardInterrupt:
            self.service.stop()
            raise KeyboardInterrupt
