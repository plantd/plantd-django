import yaml
from django.core.management import BaseCommand
from django.db import transaction

from plantd.core.models import Organization
from plantd.utils.helpers.model import create_configuration
from plantd.utils.yaml import Loader, setup_loader


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument(
            "--configuration",
            "-c",
            dest="configuration",
            required=True,
            help="The path to a YAML file containing the configuration to load",
        )
        parser.add_argument(
            "--organization-id",
            "-o",
            dest="organization-id",
            required=True,
            help="The ID of the organization to load the configuration into",
        )

    def handle(self, *args, **options):
        organization_id = options["organization-id"]
        try:
            organization = Organization.objects.get(pk=organization_id)
            data = self.__load_data(options["configuration"])
            with transaction.atomic():
                create_configuration(data, organization)
        except Organization.DoesNotExist:
            raise Exception(f"No organization was found with an ID of {organization_id}")

    @staticmethod
    def __load_data(file):
        """
        Load YAML data for the configuration to load.

        :param file: YAML file to load
        :return: Configuration data
        """
        setup_loader()
        try:
            with open(file) as f:
                f.template_data = {}
                data = yaml.load(f, Loader)
                if not data:
                    raise Exception("no input data was provided")
                if not data.get("configuration"):
                    raise Exception("the input data is missing a `configuration:` section")
                return data["configuration"]
        except FileNotFoundError:
            raise Exception(f"configuration file {file} does not exist")
