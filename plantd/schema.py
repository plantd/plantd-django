"""Schema root."""

import graphene
import graphql_jwt
from django.conf import settings
from graphene import AbstractType, ObjectType
from graphene_django.debug import DjangoDebug

import plantd.auth.schema
import plantd.core.schema
import plantd.dashboards.schema
import plantd.devices.schema
import plantd.networks.schema
from plantd.utils.graphql import get_query_cost, get_required_scopes, SerializedObject


class QueryCost(AbstractType):
    """Adds query cost to a GraphQL response"""

    cost = graphene.Field(SerializedObject, name="__cost")

    def resolve_cost(self, info):
        """
        Provide a calculated cost of a GraphQL query.

        TODO: calculation needs to be implemented.

        :param info: Query information
        """
        cost, structure = get_query_cost(
            info.schema,
            info.operation,
            getattr(info, "variable_values", {}),
            getattr(info, "fragments", {}),
        )
        return structure


class RequiredScopes(AbstractType):
    """Adds the required scopes to a GraphQL response"""

    required_scopes = graphene.List(graphene.String)

    def resolve_required_scopes(self, info):
        """
        Provide the scopes that the user needs access for. This isn't for
        anything yet, it should be implemented when API tokens have been added.

        TODO: needs to be implemented.

        :param info: Query information
        """
        return get_required_scopes(
            info.schema,
            info.operation,
            getattr(info, "variable_values", {}),
            getattr(info, "fragments", {}),
        )


class Private:
    """Private schema types."""

    class Query(
        plantd.auth.schema.Query,
        plantd.core.schema.Query,
        plantd.devices.schema.Query,
        plantd.dashboards.schema.Query,
        plantd.networks.schema.Query,
        QueryCost,
        RequiredScopes,
        ObjectType,
    ):
        """Private GraphQL Query provider."""

        if settings.DEBUG:
            debug = graphene.Field(DjangoDebug, name="__debug")

    class Mutation(
        plantd.auth.schema.Mutation,
        plantd.core.schema.Mutation,
        plantd.dashboards.schema.Mutation,
        plantd.devices.schema.Mutation,
        plantd.networks.schema.Mutation,
        RequiredScopes,
        ObjectType,
    ):
        """Private GraphQL Mutation provider."""

        pass


class Public:
    """Public schema types."""

    class Query(
        plantd.auth.schema.Query,
        QueryCost,
        ObjectType,
    ):
        """Public GraphQL Query provider."""

        if settings.DEBUG:
            debug = graphene.Field(DjangoDebug, name="__debug")

    class Mutation(
        ObjectType,
    ):
        """Public GraphQL Mutation provider."""

        token_auth = graphql_jwt.ObtainJSONWebToken.Field()
        verify_token = graphql_jwt.Verify.Field()
        refresh_token = graphql_jwt.Refresh.Field()


schema = graphene.Schema(query=Private.Query, mutation=Private.Mutation, auto_camelcase=False)

public_schema = graphene.Schema(query=Public.Query, mutation=Public.Mutation, auto_camelcase=False)
