"""URLs to handle development utilities."""
from django.urls import path

from plantd.development.views import UtilitiesView

urlpatterns = [
    path("utilities", UtilitiesView.as_view(), name="utilities"),
]
