"""Development views."""
from django.http import HttpRequest, HttpResponse
from django.shortcuts import render
from django.views import View


class UtilitiesView(View):
    """Default view for the application."""

    @staticmethod
    def get(request: HttpRequest) -> HttpResponse:
        """
        Handle GET requests for development related view.

        :param request: Incoming request
        :return: Rendered utilities page
        """
        return render(request, "utilities.html")
