"""Django models for the dashboards app."""

# flake8: noqa

from .dashboard import Dashboard
