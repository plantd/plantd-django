"""Dashboard model and associated manager classes."""
from django.db import models
from django.utils.translation import ugettext_lazy as _

DASHBOARD_NAME_MAX_LENGTH = 150


class DashboardManager(models.Manager):
    pass


class Dashboard(models.Model):
    """Dashboard model for displaying and controlling network information."""

    class Meta:
        app_label = "dashboards"

    name = models.CharField(
        _("name"),
        max_length=DASHBOARD_NAME_MAX_LENGTH,
        null=False,
        blank=True,
        default=None,
        help_text=_(f"Required. {DASHBOARD_NAME_MAX_LENGTH} characters or fewer."),
    )

    organization = models.ForeignKey(
        "core.Organization",
        related_name="dashboards",
        on_delete=models.CASCADE,
        help_text=_("Organization that the dashboard is associated with."),
    )

    objects = DashboardManager()
