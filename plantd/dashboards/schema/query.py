"""GraphQL query provider for the dashboards app."""

from graphene import AbstractType


class Query(AbstractType):
    """Dashboard queries."""

    pass
