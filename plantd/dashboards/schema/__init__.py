"""GraphQL schema for the dashboards app."""

# flake8: noqa

from .mutation import Mutation
from .query import Query
