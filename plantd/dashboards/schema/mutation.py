"""GraphQL mutation provider for the dashboards app."""

from graphene import AbstractType


class Mutation(AbstractType):
    """Dashboard mutations."""

    pass
