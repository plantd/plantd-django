"""Django models for the devices app."""

# flake8: noqa

from .device import Device
