"""Device model and associated manager classes."""
from django.db import models
from django.utils.translation import ugettext_lazy as _

from plantd.core.validators import DeviceNameValidator

# Follow domain name limits
# TODO: validate input
DEVICE_LABEL_MAX_LENGTH = 63
DEVICE_NAME_MAX_LENGTH = 253


class DeviceManager(models.Manager):
    pass


class Device(models.Model):
    """Dashboard model for displaying and controlling network information."""

    class Meta:
        app_label = "devices"

    name_validator = DeviceNameValidator()

    name = models.CharField(
        _("name"),
        max_length=DEVICE_NAME_MAX_LENGTH,
        unique=True,
        null=False,
        blank=True,
        default=None,
        help_text=_(f"Device name in the reverse domain name format. {DEVICE_NAME_MAX_LENGTH} characters or fewer."),
        validators=[name_validator],
        error_messages={"unique": _("An device with that name already exists.")},
    )

    network = models.ForeignKey(
        "networks.Network",
        related_name="devices",
        on_delete=models.CASCADE,
        help_text=_("Network that the device is connected to."),
    )

    is_connected = models.BooleanField(
        default=False,
        help_text=_("Whether or not the device is connected to the network."),
    )

    last_seen = models.DateTimeField(
        _("last seen"),
        null=True,
        blank=True,
        help_text=_("Date and time that the device was last connected."),
    )

    objects = DeviceManager()
