import factory

from plantd.devices.models import Device
from plantd.networks.factories.network import NetworkFactory


class DeviceFactory(factory.django.DjangoModelFactory):
    """Factory for constructing instances of the Device model."""

    class Meta:
        model = Device

    name = factory.Sequence(lambda n: f"network{n}")
    network = factory.SubFactory(NetworkFactory)
