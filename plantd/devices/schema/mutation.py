"""GraphQL mutation provider for the devices app."""
import graphene
from graphene import AbstractType
# from graphene_django import DjangoObjectType

from plantd.lib.message import JobRequest, JobResponse

from plantd.utils.connection_pool import pool


# class JobNode(DjangoObjectType):
class JobNode(graphene.Node):
    """GraphQL type for a job."""
    pass
    # class Meta:
    #     fields = ["id"]


class SubmitJob(graphene.Mutation):
    """Mutation to submit a new job to a broker."""

    job = graphene.Field(JobNode)

    class Arguments:
        service = graphene.String(required=True)

    def mutate(self, info, service):
        """Submit the job and return the response."""
        with pool.item() as client:
            message = JobRequest(id=service)
            request = [b"submit-job", message.to_json().encode()]
            reply = client.send(service.encode(), request)
            response = JobResponse.from_json(reply[1].decode())

            return SubmitJob(response.job)


class Mutation(AbstractType):
    """Device mutations."""

    submit_job = SubmitJob.Field()
