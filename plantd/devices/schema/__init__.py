"""GraphQL schema for the devices app."""

# flake8: noqa

from .mutation import Mutation
from .query import Query
