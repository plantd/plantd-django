"""GraphQL query provider for the devices app."""

from graphene import AbstractType


class Query(AbstractType):
    """Device queries."""

    pass
