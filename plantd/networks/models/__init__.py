"""Django models for the networks app."""

# flake8: noqa

from .network import Network
