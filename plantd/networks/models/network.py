"""Network model and associated manager classes."""
from django.db import models
from django.utils.translation import ugettext_lazy as _

NETWORK_NAME_MAX_LENGTH = 150


class NetworkManager(models.Manager):
    pass


class Network(models.Model):
    """Network model for organizing devices that perform data acquisition, logging, and control systems."""

    class Meta:
        app_label = "networks"

    name = models.CharField(
        _("name"),
        max_length=NETWORK_NAME_MAX_LENGTH,
        null=False,
        blank=True,
        default=None,
        help_text=_(f"Required. {NETWORK_NAME_MAX_LENGTH} characters or fewer."),
    )

    organization = models.ForeignKey(
        "core.Organization",
        related_name="networks",
        on_delete=models.CASCADE,
        help_text=_("Organization that the network is associated with."),
    )

    objects = NetworkManager()
