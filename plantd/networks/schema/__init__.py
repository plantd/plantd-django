"""GraphQL schema for the networks app."""

# flake8: noqa

from .mutation import Mutation
from .query import Query
