"""GraphQL query provider for the networks app."""

from graphene import AbstractType


class Query(AbstractType):
    """Network queries."""

    pass
