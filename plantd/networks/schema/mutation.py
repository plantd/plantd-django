"""GraphQL mutation provider for the networks app."""

from graphene import AbstractType


class Mutation(AbstractType):
    """Network mutations."""

    pass
