import factory

from plantd.core.factories import OrganizationFactory
from plantd.networks.models import Network


class NetworkFactory(factory.django.DjangoModelFactory):
    """Factory for constructing instances of the Network model."""

    class Meta:
        model = Network

    name = factory.Sequence(lambda n: f"network{n}")
    organization = factory.SubFactory(OrganizationFactory)
