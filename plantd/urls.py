"""Django application URLs."""
from django.conf import settings
from django.contrib import admin
from django.urls import include, path

urlpatterns = [
    path("", include("plantd.core.urls")),
    path("admin", admin.site.urls),
]

if settings.DEVELOPMENT:
    urlpatterns += [
        path("development", include("plantd.development.urls")),
    ]
