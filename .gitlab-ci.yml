include:
  - template: Jobs/Code-Quality.gitlab-ci.yml
  - template: Jobs/SAST.gitlab-ci.yml
  - template: Jobs/Secret-Detection.gitlab-ci.yml
  - template: Security/Dependency-Scanning.gitlab-ci.yml
  - template: Security/License-Scanning.gitlab-ci.yml

stages:
  - build
  - checks
  - test
  - analysis

variables:
  DOCKER_DRIVER: overlay2
  npm_config_cache: "$CI_PROJECT_DIR/.npm"
  CYPRESS_CACHE_FOLDER: "$CI_PROJECT_DIR/cache/Cypress"
  PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pip"
  POSTGRES_DB: plantd
  POSTGRES_USER: postgres
  POSTGRES_PASSWORD: postgres
  POSTGRES_HOST_AUTH_METHOD: trust

services:
  - postgres:12.2-alpine
  - name: registry.gitlab.com/plantd/broker:staging
    alias: broker
  - name: registry.gitlab.com/plantd/plantd-django/messaging:latest
    alias: messaging

cache:
  key: ${CI_COMMIT_REF_SLUG}
  paths:
    - .cache/pip/
    - .npm/
    - .venv/
    - cache/Cypress/
    - node_modules/

.install-deps-template: &install-deps
  before_script:
    - python -V  # Print out python version for debugging
    - pip install --upgrade pip
    - pip install -r requirements.txt
    - poetry config virtualenvs.create false
    - poetry install -vv
    - python manage.py migrate

.backend-template: &backend-definition
  <<: *install-deps
  image: python:3.8.10
  variables:
    DJANGO_SETTINGS_MODULE: "config.settings.ci"
    DATABASE_URL: "postgresql://postgres:postgres@postgres:5432/$POSTGRES_DB"
    G_MESSAGES_DEBUG: all
    PLANTD_BROKER_LOG_LEVEL: info
    PLANTD_MODULE_ARGS: -vvv
    PLANTD_MODULE_STANDALONE: 'false'
    PLANTD_MODULE_CONFIG_FILE: data/module.json
    PLANTD_MODULE_ENDPOINT: tcp://broker:7200
    PLANTD_MODULE_SERVICE: messaging

.frontend-template: &frontend-definition
  image: node:16.3.0-alpine
  variables:
    CHILD_CONCURRENCY: 1
  before_script:
    - apk update && apk add --no-cache bash curl python3 make g++ git
    - cd app/
    - yarn install --cache-folder .npm --frozen-lockfile --check-files

build:backend:
  <<: *backend-definition
  stage: build
  script:
    - echo "build backend"

test:backend:
  <<: *backend-definition
  stage: test
  needs: ["build:backend"]
  dependencies:
    - build:backend
  script:
    - python manage.py test

coverage:backend:
  <<: *backend-definition
  stage: analysis
  except:
    - tags
  allow_failure: true
  needs: ["test:backend"]
  dependencies:
    - test:backend
  script:
    - coverage run manage.py test
  after_script:
    - bash <(curl -s https://codecov.io/bash) -c -F backend

build:app:
  <<: *frontend-definition
  stage: build
  script:
    - yarn build

test:app:unit:
  <<: *frontend-definition
  stage: test
  needs: ["build:app"]
  script:
    - yarn jest
  artifacts:
    when: always
    paths:
      - app/coverage/

test:app:e2e:
  image: cypress/base:10
  stage: test
  needs: ["build:app"]
  script:
    - cd app/
    - yarn install
    - yarn run serve:ci &
    - yarn run test:e2e:record
  artifacts:
    when: always
    paths:
      - app/tests/e2e/videos/**/*.mp4
      - app/tests/e2e/screenshots/**/*.png
    expire_in: 1 day

coverage:app:
  <<: *frontend-definition
  stage: analysis
  except:
    - tags
  allow_failure: true
  needs: ["test:app:unit"]
  dependencies:
    - test:app:unit
  script:
    - yarn jest
  after_script:
    - curl -s https://codecov.io/bash > codecov.sh
    - bash codecov.sh -F frontend

sast:
  stage: checks

semgrep-sast:
  stage: checks
  cache: {}

nodejs-scan-sast:
  stage: checks
  cache: {}

code_quality:
  stage: checks
  cache: {}

secret_detection:
  stage: checks

dependency_scanning:
  stage: checks

gemnasium-dependency_scanning:
  stage: checks
  cache: {}

gemnasium-python-dependency_scanning:
  stage: checks
  cache: {}

license_scanning:
  stage: checks
