module.exports = {
  stories: ["../../src/**/*.stories.@(js|jsx|ts|tsx|mdx)"],
  addons: ["@storybook/addon-controls", "@storybook/addon-essentials", "@storybook/addon-links"],
};
