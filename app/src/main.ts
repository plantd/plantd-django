import Vue from "vue";
import { DefaultApolloClient } from "@vue/apollo-composable";
import VueCompositionApi, { provide } from "@vue/composition-api";

import App from "./App.vue";
import router from "./router";
import truncate from "./filters/truncate";
import { apolloClient } from "./graphql/client";

Vue.config.devtools = process.env.NODE_ENV === "development";
Vue.config.productionTip = false;

Vue.use(VueCompositionApi);

Vue.filter("truncate", truncate);

new Vue({
  setup() {
    provide(DefaultApolloClient, apolloClient);
  },
  router,
  render: (h) => h(App),
}).$mount("#app");
