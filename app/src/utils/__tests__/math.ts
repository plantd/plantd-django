import { deg2rad, rad2deg } from "@/utils/math";

describe("math utilities", () => {
  it("converts degrees to radians", () => {
    expect(deg2rad(180)).toBe(Math.PI);
  });

  it("converts radians to degrees", () => {
    expect(rad2deg(Math.PI)).toBe(180);
  });
});
