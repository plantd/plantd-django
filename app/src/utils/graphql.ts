interface GraphQLNode {
  pk?: number;
  edges?: Array<GraphQLNode>;
  node?: GraphQLNode;
}

export function flattenEdges(edges: Array<GraphQLNode>): (GraphQLNode | undefined)[] {
  return edges.map((edge) => edge.node);
}

// export function flattenEdgesToObject(arr: GraphQLNode): GraphQLNode {
//   return (
//     arr.edges &&
//     arr.edges.reduce((acc, { node }) => {
//       // if (!node.pk) {
//       //   throw new Error("Missing primary key");
//       // }
//
//       acc[node.pk] = flattenNode(node);
//       return acc;
//     }, {})
//   );
// }

// export function flattenNode(node: GraphQLNode): GraphQLNode {
//   return {};
// }
