import * as d3 from "d3";

export function minmax(data: number[]): (number | undefined)[] {
  return [d3.min(data), d3.max(data)];
}
