import { shallowMount } from "@vue/test-utils";

import BarGauge from "../index.vue";

describe("BarGauge", () => {
  const config = {
    maxValue: 100,
    customSegmentStops: [0, 50, 150],
    segmentColors: ["#bf616a", "#a3be8c"],
    textColor: "#000",
  };

  it("renders a bar gauge", () => {
    const wrapper = shallowMount(BarGauge, {
      propsData: { config },
    });
    expect(wrapper.is(BarGauge)).toBe(true);
  });
});
