import Vue from "vue";
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import VueRouter, { RouteConfig } from "vue-router";
import Home from "@/views/home.vue";

Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/about",
    name: "About",
    component: () => import(/* webpackChunkName: "core" */ "../views/about.vue"),
  },
  {
    path: "/account",
    name: "Account",
    component: () => import(/* webpackChunkName: "core" */ "../views/account.vue"),
  },
  {
    path: "/organization",
    name: "Organization",
    component: () => import(/* webpackChunkName: "core" */ "../views/organization.vue"),
  },
  {
    path: "/profile",
    name: "Profile",
    component: () => import(/* webpackChunkName: "core" */ "../views/profile.vue"),
  },
  {
    path: "/controllers",
    name: "Controllers",
    component: () => import(/* webpackChunkName: "control" */ "../views/controllers.vue"),
  },
  {
    path: "/devices",
    name: "Devices",
    component: () => import(/* webpackChunkName: "control" */ "../views/devices.vue"),
  },
];

const router = new VueRouter({
  mode: "history",
  routes,
});

// const router = new VueRouter({
//   mode: "history",
//   base: process.env.BASE_URL,
//   routes,
// });

export default router;
