export default function truncate(text: string, length: number, clamp: string | null): string | null {
  clamp = clamp || "...";
  const node = document.createElement("div");
  node.innerHTML = text;
  const content: string | null = node.textContent;
  return content && content.length > length ? content.slice(0, length) + clamp : content;
}
