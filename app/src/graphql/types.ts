export type User = {
  id: string;
  username: string;
  firstName: string;
  lastName: string;
  isStaff: boolean;
  isActive: boolean;
  isSuperuser: boolean;
  email: string;
  dateJoined: string;
  lastLogin: string;
};
