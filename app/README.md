# plantd - Web Application

## Project setup

```
yarn install
```

## Compiles and hot-reloads for development

```
yarn serve
```

## Compiles and minifies for production

```
yarn build
```

## Lints and fixes files

```
yarn lint
```

### Lint style sheets

```
yarn lint:css
```

## Testing

### Jest

```
yarn test:e2e
```

### Cypress.io

```
yarn test:e2e
```

## Storybook

### Builds for Production

```
yarn storybook:build
```

### Compiles and Serves

```
yarn storybook:serve
```
