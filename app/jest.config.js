module.exports = {
  collectCoverage: true,
  collectCoverageFrom: ["src/**/*.{ts,vue}", "!src/main.ts"],
  moduleFileExtensions: ["js", "json", "ts", "vue"],
  moduleNameMapper: {
    "^@/(.*)$": "<rootDir>/src/$1",
  },
  modulePaths: ["<rootDir>/src/", "<rootDir>/node_modules/"],
  preset: "@vue/cli-plugin-unit-jest/presets/typescript-and-babel",
  testMatch: ["**/tests/unit/**/*.spec.(js|ts)", "**/__tests__/*.(js|ts)"],
  transform: {
    "^.+\\.ts?$": "ts-jest",
    ".*\\.(vue)$": "vue-jest",
  },
  transformIgnorePatterns: ["/node_modules/(?!lodash-es)"],
};
