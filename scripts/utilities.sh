#!/bin/bash

function setup() {
    if [[ ! -d .venv ]]; then
        python3 -m venv .venv
        source .venv/bin/activate
    fi
    pip install --upgrade pip
    pip install pip-tools pre-commit
    pre-commit install
}

function prepare() {
    pip-compile --output-file=requirements.txt requirements.in
}

function activate() {
    # TODO: check venv
    # TODO: if pyenv use `pyenv activate plantd-django` else `source .venv/bin/activate`
    echo "Not implemented"
}

function deactivate() {
    # TODO: check venv
    # TODO: if pyenv use `pyenv deactivate` else `deactivate`
    echo "Not implemented"
}

prepare

# TODO: add support for --prepare
# TODO: add support for --setup
