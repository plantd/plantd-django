[![license](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)
[![black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
[![codecov](https://codecov.io/gl/plantd/plantd-django/branch/staging/graph/badge.svg?token=2HYT5UZ674)](https://codecov.io/gl/plantd/plantd-django)
[![cypress](https://img.shields.io/endpoint?url=https://dashboard.cypress.io/badge/simple/g3r2xo/staging&style=flat&logo=cypress)](https://dashboard.cypress.io/projects/g3r2xo/runs)

---

# Plantd Core

![logo]

This project is pre-alpha, some things work, but it's still too early to be useful. Please create an [issue][issue] if
there's something missing that would make this useful to you.

The `plantd` project provides a set of services and libraries to help with building control systems. This project
provides:

* User interface
* GraphQL API
* Authentication
* Configuration
* Message broker
* Task queue and job scheduler

For functionality to build individual "devices" that operate within a `plantd` network use [`libplantd`][libplantd]. It
provides:

* Application base using `GLib`
* Asynchronous job handler for generic task development
* Data provider components for data acquisition
* Logging provider components for data logging
* Finite state machines through a distributed event system

## Built With

![django]![graphql]![vue]![typescript]

## Quick Start

```shell
pip install -r requirements.txt
cp .env.development .env
docker-compose up -d
psql -U postgres -h localhost
# at the prompt enter:
# create database plantd;
./manage.py migrate
./manage.py runserver
./manage.py createsuperuser
```

To login go to http://localhost:8000/admin. <br /><br />
A GraphQL API should be available at http://localhost:8000/graphql that can be interacted with using the GraphiQL IDE.

## Additional Documentation

See [here][docs].

## License

See the [LICENSE][license] file.

<!-- links -->
[logo]: docs/assets/logo.png
[issue]: https://gitlab.com/plantd/plantd-django/-/issues/new
[libplantd]: https://gitlab.com/plantd/libplantd
[docs]: docs/docs.md
[django]: docs/assets/django.png
[graphql]: docs/assets/graphql.png
[vue]: docs/assets/vue.png
[typescript]: docs/assets/typescript.png
[license]: LICENSE
