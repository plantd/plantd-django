"""Documentation specific overrides for the plantd project."""
# flake8: noqa

from config.settings.production import *

# Could use a mixin to override the log file output location that causes this
# to fail, but logging isn't necessary in the documentation.
LOGGING = {}
