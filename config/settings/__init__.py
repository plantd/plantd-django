"""Django application settings files.

Configurations for:
- production
- development

To extend development for a custom local environment create a file `local.py` and add to the top:

    from config.settings.development import *

and then start the server with `DJANGO_SETTINGS_MODULE=config.settings.local ./manage runserver`.
"""
