"""Testing settings file.

This should contain customized configuration settings intended for testing.
"""
# flake8: noqa
import os

from config.settings.development import *
from plantd.utils import is_true

PRODUCTION = is_true(os.environ.get("PRODUCTION", "false"))


DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql_psycopg2",
        "NAME": "plantd",
        "USER": "postgres",
        "PASSWORD": "postgres",
        "HOST": "localhost",
        "PORT": "",
    }
}
