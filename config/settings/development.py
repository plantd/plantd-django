"""
Development settings for the plantd project.
"""
# flake8: noqa
import os

from config.settings.production import *
from plantd.utils import is_true

# from plantd.utils.debug import print_queries

DEBUG = is_true(os.getenv("DEBUG", "true"))

# This should never be true in development
PRODUCTION = is_true(os.getenv("PRODUCTION", str(not DEBUG)))
DEVELOPMENT = True

INSTALLED_APPS += ("plantd.development",)

BROKER = {
    "endpoint": "tcp://localhost:7200",
}
