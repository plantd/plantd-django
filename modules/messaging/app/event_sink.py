import os

import gi

gi.require_version("Apex", "1.0")

from gi.repository import Apex  # noqa: E402


class EventSink(Apex.Sink):
    __gtype_name__ = "EventSink"

    def __init__(self, *args, **kwargs):
        endpoint = os.getenv("TEST_EVENTS_BACKEND", "tcp://localhost:12000")
        super().__init__(*args, **kwargs)
        self.set_endpoint(endpoint)
        self.set_filter("")

    def do_handle_message(self, msg):
        event = Apex.Event.new()
        if event is None:
            raise Exception("failed to create an event")
        # event.deserialize(msg)
        Apex.debug(msg)
