from django.test import TestCase

from plantd.core.factories import ObjectFactory, ProfileFactory, PropertyFactory
from plantd.utils.test.graphql import run_query


class PropertyTest(TestCase):
    properties_fragment = """
    fragment PropertiesFragment on PropertyUnionConnection {
        edges {
            node {
                ... on IntProperty {
                    key
                    ival:value
                }
                ... on StringProperty {
                    key
                    sval:value
                }
                ... on FloatProperty {
                    key
                    fval:value
                }
                ... on BoolProperty {
                    key
                    bval:value
                }
                __typename
            }
        }
    }
    """

    def setUp(self):
        self.profile = ProfileFactory.create()
        self.object = ObjectFactory.create(creator=self.profile, name="test-object")
        self.iprop = PropertyFactory.create(object=self.object, key="int-prop", value=99)
        self.sprop = PropertyFactory.create(object=self.object, key="string-prop", value="derp")
        self.fprop = PropertyFactory.create(object=self.object, key="float-prop", value=12.34)
        self.bprop = PropertyFactory.create(object=self.object, key="bool-prop", value=True)
        self.query = """
        query get_objects($pk: Int!) {
            object(pk: $pk) {
                properties {
                    ... PropertiesFragment
                }
            }
        }
        """

    def test_property_type(self):
        result = run_query(
            self.profile,
            self.query,
            fragments=[self.properties_fragment],
            variables={"pk": self.object.pk},
        )
        properties = [edge["node"] for edge in result["object"]["properties"]["edges"]]
        self.assertEqual(len(properties), 4)
        self.assertEqual(properties[0]["__typename"], "IntProperty")
        self.assertEqual(properties[0]["key"], self.iprop.key)
        self.assertEqual(properties[0]["ival"], self.iprop.value)
        self.assertEqual(properties[1]["__typename"], "StringProperty")
        self.assertEqual(properties[1]["key"], self.sprop.key)
        self.assertEqual(properties[1]["sval"], self.sprop.value)
        self.assertEqual(properties[2]["__typename"], "FloatProperty")
        self.assertEqual(properties[2]["key"], self.fprop.key)
        self.assertEqual(properties[2]["fval"], self.fprop.value)
        self.assertEqual(properties[3]["__typename"], "BoolProperty")
        self.assertEqual(properties[3]["key"], self.bprop.key)
        self.assertEqual(properties[3]["bval"], self.bprop.value)
