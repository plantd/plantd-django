from django.test import TestCase

from plantd.core.factories import ProfileFactory, UserFactory
from plantd.utils.test.graphql import run_query


class ProfileTest(TestCase):
    def setUp(self):
        user = UserFactory.create(
            first_name="Foo",
            last_name="Bar",
            email="foobar@domain.net",
        )
        self.profile = ProfileFactory.create(user=user)

    def test_query_profile(self):
        query = """
        query get_profile($pk: Int!) {
            profile(pk: $pk) {
                full_name
                first_name
                last_name
            }
        }
        """

        result = run_query(self.profile, query, variables={"pk": self.profile.pk})
        self.assertEqual(result["profile"]["full_name"], self.profile.full_name)
        self.assertEqual(result["profile"]["first_name"], self.profile.first_name)
        self.assertEqual(result["profile"]["last_name"], self.profile.last_name)
