from django.test import TestCase

from plantd.core.factories import ObjectFactory, ProfileFactory, PropertyFactory
from plantd.utils.test.graphql import run_query


class ObjectTest(TestCase):
    properties_fragment = """
    fragment PropertiesFragment on PropertyUnionConnection {
        edges {
            node {
                ... on IntProperty {
                    key
                    ival:value
                }
                ... on StringProperty {
                    key
                    sval:value
                }
                ... on FloatProperty {
                    key
                    fval:value
                }
                ... on BoolProperty {
                    key
                    bval:value
                }
                __typename
            }
        }
    }
    """

    def setUp(self):
        self.profile = ProfileFactory.create()

    def test_query_object_tree(self):
        root = ObjectFactory(creator=self.profile)
        prop1 = PropertyFactory(object=root, key="count", value=10)
        prop2 = PropertyFactory(object=root, key="ref", value="node1")
        query = """
        query get_objects($pk: Int!) {
            object(pk: $pk) {
                properties {
                    ... PropertiesFragment
                }
                objects {
                    edges {
                        node {
                            properties {
                                ...PropertiesFragment
                            }
                        }
                    }
                }
            }
        }
        """

        result = run_query(self.profile, query, fragments=[self.properties_fragment], variables={"pk": root.pk})
        properties = [edge["node"] for edge in result["object"]["properties"]["edges"]]
        self.assertEqual(len(properties), 2)
        self.assertEqual(properties[0]["__typename"], "IntProperty")
        self.assertEqual(properties[0]["key"], prop1.key)
        self.assertEqual(properties[0]["ival"], prop1.value)
        self.assertEqual(properties[1]["__typename"], "StringProperty")
        self.assertEqual(properties[1]["key"], prop2.key)
        self.assertEqual(properties[1]["sval"], prop2.value)
