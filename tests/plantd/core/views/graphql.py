from django.test import TestCase

from plantd.core.factories import ProfileFactory
from plantd.core.factories.user import default_password


class GraphQLTest(TestCase):
    """GraphQL view tests."""

    def setUp(self):
        self.profile = ProfileFactory()
        self.assertTrue(self.client.login(username=self.profile.user.username, password=default_password))

    def test_profile_query(self):
        """TODO: something"""
        self.assertIsNotNone(self.profile)
