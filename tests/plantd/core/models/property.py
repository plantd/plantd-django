from django.test import TestCase

from plantd.core.factories import ObjectFactory, ProfileFactory, PropertyFactory
from plantd.core.models.property import PropertyType


class PropertyTest(TestCase):
    def setUp(self):
        self.profile = ProfileFactory.create()
        self.object = ObjectFactory.create(creator=self.profile)
        self.property = PropertyFactory.create(object=self.object)

    def test_property_int_value(self):
        self.property.value = 10
        self.property.save()
        self.assertTrue(isinstance(self.property.value, int))
        self.assertEqual(self.property.value_type, PropertyType.INTEGER.value)

    def test_property_str_value(self):
        self.property.value = "foo"
        self.property.save()
        self.assertTrue(isinstance(self.property.value, str))
        self.assertEqual(self.property.value_type, PropertyType.STRING.value)

    def test_property_float_value(self):
        self.property.value = 12.34
        self.property.save()
        self.assertTrue(isinstance(self.property.value, float))
        self.assertEqual(self.property.value_type, PropertyType.FLOAT.value)

    def test_property_bool_value(self):
        self.property.value = False
        self.property.save()
        self.assertTrue(isinstance(self.property.value, bool))
        self.assertEqual(self.property.value_type, PropertyType.BOOLEAN.value)
