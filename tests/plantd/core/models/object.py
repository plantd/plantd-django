from django.test import TestCase

from plantd.core.factories import ObjectFactory, ProfileFactory


class PropertyTest(TestCase):
    def setUp(self):
        self.profile = ProfileFactory.create()
        self.parent = ObjectFactory.create(creator=self.profile)

    def test_object_children(self):
        children = [ObjectFactory.create(creator=self.profile, parent=self.parent) for _ in range(3)]
        self.assertEqual(self.parent.children.count(), len(children))
        self.assertTrue(self.parent.has_children)
        for child in children:
            self.assertTrue(child.has_parent)

    def test_object_siblings(self):
        children = [ObjectFactory.create(creator=self.profile, parent=self.parent) for _ in range(3)]
        for sibling in children[0].siblings:
            self.assertEqual(children[0].parent, sibling.parent)

    # def test_object_ancestor(self):
    #     grandparent = ObjectFactory.create(creator=self.profile)
    #     parent = ObjectFactory.create(creator=self.profile, parent=grandparent)
    #     child = ObjectFactory.create(creator=self.profile, parent=parent)
    #     self.assertTrue(parent.is_ancestor(child))
    #     self.assertTrue(grandparent.is_ancestor(child))

    def test_object_descendant(self):
        grandparent = ObjectFactory.create(creator=self.profile)
        parent = ObjectFactory.create(creator=self.profile, parent=grandparent)
        child = ObjectFactory.create(creator=self.profile, parent=parent)
        self.assertTrue(child.is_descendant(parent))
        self.assertTrue(child.is_descendant(grandparent))
