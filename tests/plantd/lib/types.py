from django.test import TestCase

from plantd.lib.types import Object, Property


class TypeTest(TestCase):
    def test_types(self):
        json = """{
            "id": "obj_a",
            "properties": [{
                "key": "str_prop", "value": "foo"
            }, {
                "key": "int_prop", "value": 123
            }, {
                "key": "float_prop", "value": 1.23
            }, {
                "key": "bool_prop", "value": true
            }],
            "objects": [{
                "id": "obj_b",
                "objects": [{
                    "id": "obj_c",
                    "properties": [{
                        "key": "prop", "value": "bar"
                    }]
                }, {
                    "id": "obj_d"
                }]
            }]
        }"""
        obj = Object.from_json(json)
        self.assertEquals(obj.id, "obj_a")
        self.assertEquals(len(obj.properties), 4)
        self.assertEquals(len(obj.objects), 1)
        self.assertEquals(len(obj.objects[0].objects), 2)
        self.assertEquals(len(obj.objects[0].objects[0].properties), 1)
