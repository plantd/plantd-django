import re

from django.test import TestCase

from plantd.lib.message import JobRequest, JobResponse
from plantd.lib.types import Job


class JobRequestTest(TestCase):
    def test_serialize(self):
        message = JobRequest(
            id="test",
            job_id="test-job",
            job_value="test-value",
            job_properties=[],
        )
        json = message.to_json()
        self.assertIsNotNone(re.match("{.*}", json))
        self.assertIsNotNone(re.match('.*"id":."test".*', json))
        self.assertIsNotNone(re.match('.*"job-id":."test-job".*', json))
        self.assertIsNotNone(re.match('.*"job-value":."test-value".*', json))
        self.assertIsNotNone(re.match('.*"job-properties":.\[\].*', json))

    def test_deserialize(self):
        json = """{
            "id": "test",
            "job-id": "test-job",
            "job-value": "test-value"
        }"""
        request = JobRequest.from_json(json)
        self.assertEquals(request.id, "test")
        self.assertEquals(request.job_id, "test-job")
        self.assertEquals(request.job_value, "test-value")


class JobResponseTest(TestCase):
    def test_serialize(self):
        job = Job(id="test", priority=4)
        message = JobResponse(job)
        json = message.to_json()
        self.assertIsNotNone(re.match('{.*"job":.*{.*}.*}', json))
        self.assertIsNotNone(re.match(f'.*"id":."test".*', json))
        self.assertIsNotNone(re.match('.*"priority":.4.*', json))

    def test_deserialize(self):
        json = """{
            "job": {
                "id": "test",
                "priority": 4
            }
        }"""
        response = JobResponse.from_json(json)
        self.assertEquals(response.job.id, "test")
        self.assertEquals(response.job.priority, 4)
