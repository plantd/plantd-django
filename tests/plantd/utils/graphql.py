from django.test import TestCase

from plantd.utils.graphql import get_query_cost


class GraphQLUtilsTest(TestCase):
    def setUp(self):
        pass

    def test_get_query_cost(self):
        # TODO: implement the test when the calculation has been added
        result = get_query_cost(None, None, None, None)
        self.assertEqual(result[0], 0)
        self.assertEqual(result[1], {"__total": 0})
