import re
from functools import partial

from django.conf import settings
from django.test import TestCase
from zapi import mdp

from plantd.lib.message import JobRequest
from plantd.utils.connection_pool import ConnectionPool, TooManyConnections
from plantd.utils.test.decorators import ci_skip


class ConnectionPoolTest(TestCase):
    def setUp(self):
        self.pool = ConnectionPool(create=partial(mdp.Client, broker=settings.BROKER["endpoint"]))

    @ci_skip
    def test_pool_client(self):
        with self.pool.item() as client:
            message = JobRequest(
                id="test",
                job_id="test-job",
                job_value="test-value",
                job_properties=[],
            )
            request = [b"submit-job", message.to_json().encode()]
            reply = client.send(b"messaging", request)
            self.assertEquals(len(reply), 2)
            self.assertEquals(reply[0].decode(), "submit-job")
            body = re.sub("[\r\n\\s]*", "", reply[1].decode())
            pattern = r'{"job":{"id":"\w{8}-\w{4}-\w{4}-\w{4}-\w{12}","priority":0}}'
            self.assertIsNotNone(re.match(pattern, body))

    @ci_skip
    def test_too_many_connections(self):
        count = 3
        pool = ConnectionPool(
            create=partial(mdp.Client, broker=settings.BROKER["endpoint"]),
            max_size=count,
            block=False,
        )
        with self.assertRaises(TooManyConnections):
            for i in range(count + 1):
                pool.item()
