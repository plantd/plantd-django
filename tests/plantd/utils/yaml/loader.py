import os
import pathlib
from io import StringIO

import yaml
from django.test import TestCase

from plantd.networks.factories.network import NetworkFactory
from plantd.utils.yaml import Loader, setup_loader


class LoaderTest(TestCase):
    def setUp(self):
        setup_loader()

    def test_include(self):
        filename = pathlib.Path(os.path.dirname(__file__)) / "include.yaml"
        with open(filename) as file:
            content = yaml.load(file, Loader)
            data = content["data"]
            self.assertEqual(data[0]["one"]["foo"], "foo")
            self.assertEqual(data[0]["one"]["bar"], 42)
            self.assertFalse(data[0]["one"]["baz"])
            self.assertEqual(data[1]["two"]["foo"], "oof")
            self.assertEqual(data[1]["two"]["bar"], 24)
            self.assertTrue(data[1]["two"]["baz"])

    def test_include_dir(self):
        filename = pathlib.Path(os.path.dirname(__file__)) / "include-dir.yaml"
        with open(filename) as file:
            content = yaml.load(file, Loader)
            data = content["data"]
            self.assertEqual(data[0]["one"]["foo"], "foo")
            self.assertEqual(data[0]["one"]["bar"], 42)
            self.assertFalse(data[0]["one"]["baz"])
            self.assertEqual(data[1]["two"]["foo"], "oof")
            self.assertEqual(data[1]["two"]["bar"], 24)
            self.assertTrue(data[1]["two"]["baz"])

    def test_template_string(self):
        network = NetworkFactory(name="example")
        config = """
        setup: !tmpl
        fields:
          object: ${first}.${last}@${network.name}.com
          # check that two is the max depth
          invalid: ${first.middle.last}
          single: ${first}
        """
        stream = StringIO(config)
        stream.template_data = {
            "first": "burnt",
            "middle": None,
            "last": "chrysler",
            "network": network,
        }
        data = yaml.load(stream, Loader)["fields"]
        self.assertEqual(data["object"], "burnt.chrysler@example.com")
        self.assertEqual(data["invalid"], "${first.middle.last}")
        self.assertEqual(data["single"], "burnt")

    def test_env(self):
        os.environ["PLANTD_TEST"] = "testing"
        config = """
        env: !env $(PLANTD_TEST)
        """
        stream = StringIO(config)
        data = yaml.load(stream, Loader)
        self.assertEqual(data["env"], "testing")
